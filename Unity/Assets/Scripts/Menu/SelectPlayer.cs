﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectPlayer : MonoBehaviour 
{
	public GameObject[] Players;
	public GameObject[] Buttons;
	
	public float TimeWait = 1f;
	public GameObject Flash;
	public GameObject Cache;
	public GameObject Menu;

	public Transform Tower;

	private List<GameObject> m_players = new List<GameObject>();
	private float m_countTimer = 0f;
	private bool m_ready = false;
	private int m_countPlayers = 0;

	private InfoManager m_manager;
	private const float HEIGHT_STAGE = 160f;

	// Use this for initialization
	IEnumerator Start () 
	{
		// info & input manager
		while(!MasterManager.Instance.IsReady)
		{
			yield return new WaitForEndOfFrame();
		}

		m_manager = InfoManager.Instance;
#if UNITY_EDITOR
//		if (m_manager.CountPlayerPlaying() == 0)
//		{
//			m_manager.ActivePlayers(0);
//			m_manager.ActivePlayers(1);
//			//m_manager.SelectPlayer(2);
//		}
#endif

		if (m_manager.CountPlayerPlaying() > 0)
		{
			DesactiveMenu();
			//Flash.SetActive(true);

			int i = 0;
			List<GameObject> NextStage = new List<GameObject>();
			// check if all players haven't the current stage 
			foreach(PlayerInfo player in m_manager.Players)
			{
				// get player with current stage, if alone get the looser
				if (player.Selected && player.Stage == m_manager.CurrentStage)
				{
					Players[i].SetActive(true);
					m_players.Add(Players[i]);
				}
				// active player playing with next stage and set position
				else if (player.Playing)
				{
					Players[i].SetActive(true);
					Players[i].transform.localPosition = new Vector3(0,HEIGHT_STAGE,0);
					NextStage.Add(Players[i]);
				}
				++i;
			}
			i = 0;
			// next stage
			if (m_players.Count == 0)
			{
				m_manager.NextStage();
				Tower.localPosition -= new Vector3(0,HEIGHT_STAGE * m_manager.CurrentStage,0);
				foreach(GameObject p in Players)
					p.transform.localPosition = Vector3.zero;
				m_players = NextStage;
			}
			// add players (2 or 1 + looser) for the next game
			else if (m_players.Count == 1)
			{
				foreach(PlayerInfo player in m_manager.Players)
				{
					if (player.Playing == true && player.Win == false)
					{
						m_players.Add(Players[i]);
					}					
					++i;
				}
			}
			RandomPlayers();
		}
		else
		{
//			foreach(GameObject p in Players)
//				p.SetActive(false);
			Debug.Log("fisrt game");
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateInput();
		
		if (m_ready)
		{
			m_countTimer += TimeManager.DeltaTime;
			if (m_countTimer > TimeWait)
			{
				m_ready = false;
				RandomPlayers();
			}
		}
	}
	
	void RandomPlayers()
	{
		while (m_players.Count > 2)
		{
			int rand = RandomManager.Range(0, m_players.Count - 2);
			m_players.RemoveAt(rand);
		}
	
		m_manager.ResetPlayers();

		foreach(GameObject player in m_players)
		{
			player.animation.Play();
			int id = 0;
			int.TryParse(player.transform.GetChild(0).name, out id);
			m_manager.ActivePlayers(id-1);
		}
		
		StartCoroutine(LaunchScene());
		Invoke("Black", 1.1f);
	}
	
	IEnumerator LaunchScene()
	{
		yield return new WaitForSeconds(2.5f);
		Application.LoadLevel("Modification");
	}
	
	void ActivePlayers()
	{
		Invoke("DesactiveMenu", 0.5f);
		Flash.SetActive(true);
		
		for (int i = 0; i < Players.Length; ++i)
		{
			if (Players[i].activeSelf)
			{
				m_players.Add(Players[i]);
				m_manager.SelectPlayer(i);
			}
		}
		m_ready = true;
	}
	
	void UpdateInput()
	{
		for(int i = 0; i < InfoManager.MAX_PLAYERS; ++i)
		{
			if(InputManager.Instance[i].PressA())
			{
				if (!Players[i].activeSelf)
				{
					++m_countPlayers;
					Buttons[i].SendMessage("OnSelect", true);
					Players[i].SetActive(true);
				}
			}
			else if(InputManager.Instance[i].PressB())
			{
				if (Players[i].activeSelf)
				{
					--m_countPlayers;
					Buttons[i].SendMessage("OnSelect", false);
					Players[i].SetActive(false);
				}
			}
		}
		
		if (InputManager.Instance[0].PressStart())
		{
			if (m_countPlayers > 1)
				ActivePlayers();
		}
	}
	
	void DesactiveMenu()
	{
		Menu.SetActive(false);
	}
	
	void Black()
	{
		Cache.SetActive(true);
	}
}
