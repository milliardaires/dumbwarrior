﻿using UnityEngine;
using System.Collections;

public class BorderGroup : MonoBehaviour 
{
	public KeyCode Key;
	public UIPanel Parent;

	void OnKey (KeyCode key)
	{
		if (NGUITools.GetActive(this))
		{
			if (key == Key)
			{
				Parent.depth = -1;
			}
		}
	}

	void OnSelect (bool selected)
	{
		if (selected)
		{
			Parent.depth = 0;
		}
	}
}
