﻿using UnityEngine;
using System.Collections;

public class EndSelection : MonoBehaviour 
{
	public GameObject[] DestroyObjects;
	//public GameObject Top;
	public GameObject Cache;
	//public Animation m_AnimationCache = null;

	public StartSelection[] Players;	

	private bool m_ready = false;
	private int m_count = 0;

	// Update is called once per frame
	void Update () 
	{
		if (!m_ready)
		{
			m_count = 0;
			foreach(StartSelection player in Players)
			{
				if (player.Ready)
					++m_count;
			}

			if (m_count == Players.Length)
			{
				m_ready = true;
				Invoke("Load", 3f); // 5f
				Invoke("Black", 1f); // 3f
			}
		}
	}

	void Load()
	{
		foreach(GameObject go in DestroyObjects)
			Destroy(go);
		Application.LoadLevel("LoadScreen");
		//Top.SetActive(true);
		//m_AnimationCache.Play();
		//GameTick.Instance.Begin();
	}

	void Black()
	{
		Cache.SetActive(true);
	}
}
