﻿using UnityEngine;
using System.Collections;

public class LootMenu : MonoBehaviour 
{
	public string StartKey;

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButtonDown(StartKey))
		{
			Destroy(GameObject.Find("UI"));
			Application.LoadLevel("Menu");
		}
	}
}
