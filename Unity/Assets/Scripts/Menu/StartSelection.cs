﻿using UnityEngine;
using System.Collections;

public class StartSelection : MonoBehaviour 
{
	public GameObject WindowBack;
	public GameObject WindowReady;

	public int id;
	private int m_realId;
	private PlayerInfo m_player;

	public bool Ready = false;

	// Use this for initialization
	IEnumerator Start () 
	{
		//DontDestroyOnLoad(transform.root);

		while(!MasterManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

#if UNITY_EDITOR
		if (InfoManager.Instance.CountPlayerPlaying() == 0)
		{
			InfoManager.Instance.ActivePlayers(0);
			InfoManager.Instance.ActivePlayers(1);
		}
#endif

		m_realId = InfoManager.Instance.GetIdPlayer(id);
		m_player = InfoManager.Instance.GetPlayer(id);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (InputManager.Instance[m_realId].PressStart())
		{
			if (m_player.CountUnit > 0)
			{
				GoReady();
			}
			else
			{
				Debug.Log("no units selected");
			}
		}

//		if (Input.GetButtonDown(BackKey))
//			GoBack();
	}

	void GoReady()
	{
		Ready = true;
		WindowReady.SetActive(true);
		WindowBack.SetActive(false);
	}
}
