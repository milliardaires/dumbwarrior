﻿using UnityEngine;
using System.Collections;
using System;

public class UiIButton : MonoBehaviour 
{
	public bool Master = true;
	public bool Fill = false;
	
	public UI2DSprite Icon;
	public Sprite DefaultSprite;

	protected virtual void OnClick ()
	{
		Debug.Log("Action 1 ");
	}

	public virtual void Action2()
	{
		Debug.Log("Action 2 ");
	}

	public virtual void Refresh()
	{}

	public void AddSprite(Sprite sprite)
	{
		Fill = true;
		Icon.sprite2D = sprite;
	}

	public void ResetSprite()
	{
		Fill = false;
		Icon.sprite2D = DefaultSprite;
	}
}
