﻿using UnityEngine;
using System.Collections;
using System;

public class UIItems : UiIButton 
{
	private Item m_item;
	public string ItemName
	{
		get { return m_item.Name;}
	}

	private UiInfoItems m_info;

	public event Action<Item> OnSell;
	public event Action<UIItems, Item, bool> OnClic;

	public void Init(UiInfoItems info, Item item)
	{
		m_info = info;
		m_item = item;
		if (m_item != null)
			Icon.sprite2D = m_item.Icon;
	}

	protected override void OnClick ()
	{
		if (!Master && m_item != null)
		{
			if (OnClic != null)
				OnClic(this, m_item, true);
		}
		else
		{
			if (Fill)
			{
				if (OnClic != null)
					OnClic(this, m_item, false);
				RemoveItem();
			}
		}
	}

	void OnSelect(bool select)
	{
		if (select)
		{
			m_info.UpdateUI(m_item);
		}
	}

	public override void Action2()
	{
		if (m_item != null)
		{
			if (OnSell != null)
			{
				OnSell(m_item);
				RemoveItem();
				Refresh();
			}
		}
	}

	public void ReplaceItem(Item item)
	{
		m_item = item;
		AddSprite(m_item.Icon);
		Refresh();
	}

	public void RemoveItem()
	{
		m_item = null;
		ResetSprite();
	}

	public void AddOnMaster(Item item)
	{
		m_item = item;
		AddSprite(item.Icon);
	}

	public override void Refresh ()
	{
		m_info.UpdateUI(m_item);
	}
}
