﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UiInfoGamePlayer : MonoBehaviour
{
	public int player = 0;

	public UI2DSprite ColorPlayer;
	public UI2DSprite DrapPlayer;
	public UI2DSprite LifePlayer;

	public GameObject Units;

	private PlayerInfo m_player;
	protected UiUnitGame[] m_buttons;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!MasterManager.Instance.IsReady)
		{
			yield return new WaitForEndOfFrame();
		}
#if UNITY_EDITOR
		if (InfoManager.Instance.CountPlayerPlaying() == 0)
		{
			InfoManager.Instance.ActivePlayers(0);
			InfoManager.Instance.ActivePlayers(1);
		}
#endif
		m_player = InfoManager.Instance.GetPlayer(player);
		//m_player.AddCurrentUnit(m_player.Units[0]);

		FillUnits();

		ColorPlayer.sprite2D = InfoManager.Instance.GetColorPlayer(player);
		DrapPlayer.sprite2D = InfoManager.Instance.GetDrapPlayer(player);
		LifePlayer.sprite2D = InfoManager.Instance.GetLifeColorPlayer(player);
	}
	
	public void FillUnits()
	{
		m_buttons = Units.GetComponentsInChildren<UiUnitGame>();
		m_buttons = m_buttons.Select( x => x).OrderBy(x => x.name).ToArray();

		for (int i = 0; i < m_buttons.Length; ++i)
		{
			Unit unit = m_player.GetCurrentUnit(i);
			if (unit != null)
				m_buttons[i].Init(unit.SpriteUI, unit.GetStatValue(DataBase.TypeStats.COST));
		}
	}
}
