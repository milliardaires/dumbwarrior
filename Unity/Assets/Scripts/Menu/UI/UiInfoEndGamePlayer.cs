﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UiInfoEndGamePlayer : MonoBehaviour
{
	public int player = 0;

	public UIAtlas[] Backgrounds;
	public Sprite[] BackIcons;

	public UISprite Background;
	public UI2DSprite Drap;
	public UI2DSprite BackIcon;

	public UILabel Death;
	public UILabel Diams;
	public UILabel Kill;
	public UILabel Xp;

	public GameObject Units;

	private PlayerInfo m_player;
	protected UiIButton[] m_buttons;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!MasterManager.Instance.IsReady)
		{
			yield return new WaitForEndOfFrame();
		}
#if UNITY_EDITOR
		if (InfoManager.Instance.CountPlayerPlaying() == 0)
		{
			InfoManager.Instance.ActivePlayers(0);
			InfoManager.Instance.ActivePlayers(1);
		}
#endif
		m_player = InfoManager.Instance.GetPlayer(player);
		//m_player.AddCurrentUnit(m_player.Units[0]);

		FillStats();
		FillUnits();

		Background.atlas = m_player.Win ? Backgrounds[0] : Backgrounds[1];
		Drap.sprite2D = InfoManager.Instance.GetDrapPlayer(player);
		BackIcon.sprite2D = m_player.Win ? BackIcons[0] : BackIcons[1];
	}

	void Update()
	{
		if (InputManager.Instance[m_player.Id].PressA())
		{
			Application.LoadLevel("Menu");
		}
	}

	private void FillStats()
	{
		Death.text = m_player.Death.ToString();
		Diams.text = m_player.DiamsUsed.ToString();
		Kill.text = m_player.Kills.ToString();
		Xp.text = m_player.XpWin.ToString();
	}

	private void FillUnits()
	{
		m_buttons = Units.GetComponentsInChildren<UiIButton>();
		m_buttons = m_buttons.Select( x => x).OrderBy(x => x.name).ToArray();

		int count = 0;
		if (m_player.Win)
			count = RandomManager.Range(3,5);
		else
			count = RandomManager.Range(1,2);

		for (int i = 0; i < count; ++i)
		{
			Item container = ItemsManager.Instance.GetContainer();
			m_player.AddNewItem(container);
			m_buttons[i].AddSprite(container.Icon);
		}
	}
}
