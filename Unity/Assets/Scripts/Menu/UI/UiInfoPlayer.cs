﻿using UnityEngine;
using System.Collections;

public class UiInfoPlayer : MonoBehaviour
{
	public int player = 0;

	public UILabel Diams;
	public UILabel Gold;

	public UI2DSprite ColorPlayer;
	public UI2DSprite DrapPlayer;
	private PlayerInfo m_player;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!MasterManager.Instance.IsReady)
		{
			yield return new WaitForEndOfFrame();
		}
#if UNITY_EDITOR
		if (InfoManager.Instance.CountPlayerPlaying() == 0)
		{
			InfoManager.Instance.ActivePlayers(0);
			InfoManager.Instance.ActivePlayers(1);
		}
#endif
		m_player = InfoManager.Instance.GetPlayer(player);

		Refresh();
		ColorPlayer.sprite2D = InfoManager.Instance.GetColorPlayer(player);
		DrapPlayer.sprite2D = InfoManager.Instance.GetDrapPlayer(player);
	}
	
	public void Refresh()
	{
		Diams.text = m_player.Xp.ToString();
		Gold.text = m_player.Gold.ToString();
	}
}
