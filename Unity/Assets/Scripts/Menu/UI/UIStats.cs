﻿using UnityEngine;
using System.Collections;
using System;

public class UIStats : UiIButton 
{
	private PlayerStat m_stat;
	private UiInfoStats m_info;

	public UILabel Level;

	public event Action<PlayerStat> OnClic;

	public void Init(UiInfoStats info, PlayerStat stat)
	{
		m_info = info;
		m_stat = stat;
		DisplayLevel();
	}

	protected override void OnClick ()
	{
		// TODO : level up
		if (OnClic != null)
			OnClic(m_stat);

		DisplayLevel();
		Refresh();
	}

	void OnSelect(bool select)
	{
		if (select)
		{
			m_info.UpdateUI(m_stat);
		}
	}

	public override void Action2()
	{
		if (m_stat != null)
			Debug.Log("to do sell stats ? ");
	}

	private void DisplayLevel()
	{
		Level.text = "lvl " + (m_stat.Level + 1);
	}

	public override void Refresh ()
	{
		m_info.UpdateUI(m_stat);
	}
}
