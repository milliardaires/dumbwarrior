﻿using UnityEngine;
using System.Collections;
using System;

public class UIUnits : UiIButton 
{
	private Unit m_unit;
	private UiInfoUnits m_info;

	public event Action<Unit, bool> OnClic;
	public event Action<Unit> OnBuy;

	public void Init(UiInfoUnits info, Unit unit)
	{
		m_info = info;
		m_unit = unit;
		if (m_unit != null)
		{
			Icon.sprite2D = m_unit.SpriteUI;
			if (!unit.Available)
				Icon.color = Color.grey;
		}
	}

	protected override void OnClick ()
	{
		if (!Master && m_unit != null)
		{
			if (m_unit.Available && OnClic != null)
				OnClic(m_unit, true);
		}
		else
		{
			if (Fill)
			{
				if (OnClic != null)
				{
					OnClic(m_unit, false);
					RemoveUnit();
				}
			}
		}
	}

	void OnSelect(bool select)
	{
		if (select)
		{
			m_info.UpdateUI(m_unit);
		}
	}

	public override void Action2()
	{
		if (m_unit != null && !m_unit.Available)
		{
			if (OnBuy != null)
			{
				OnBuy(m_unit);
				if (m_unit.Available)
					Icon.color = Color.white;
			}
		}
	}

	private void RemoveUnit()
	{
		m_unit = null;
		ResetSprite();
	}

	public void AddOnMaster(Unit unit)
	{
		m_unit = unit;
		AddSprite(m_unit.SpriteUI);
	}

	public override void Refresh ()
	{
		m_info.UpdateUI(m_unit);
	}
}
