﻿using UnityEngine;
using System.Collections;

public class UiUnitGame : MonoBehaviour
{
	public UI2DSprite Icon;
	public GameObject Price;

	public void Init(Sprite sprite, int cost)
	{
		Icon.sprite2D = sprite;
		Price.transform.GetChild(cost - 1).gameObject.SetActive(true);
	}
}
