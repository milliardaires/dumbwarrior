﻿using UnityEngine;
using System.Collections;

public class UiGameDiam : MonoBehaviour
{
	public Sprite DiamLevelUp;
	private Sprite InitDiam;

	private UI2DSprite m_sprite;

	// Use this for initialization
	public void Init () 
	{
		m_sprite = GetComponent<UI2DSprite>();
		InitDiam = m_sprite.sprite2D;
	}
	
	public void LevelUp()
	{
		m_sprite.sprite2D = DiamLevelUp;
	}

	public void ResetLevel()
	{
		m_sprite.sprite2D = InitDiam;
	}
}
