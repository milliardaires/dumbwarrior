﻿using UnityEngine;
using System.Collections;

public class UiInfoItems : MonoBehaviour 
{
	public UILabel Description;
	public UILabel Title;
	public UILabel Gold;

	public UI2DSprite[] Icons;
	private UILabel[] TextIcons;

	public Sprite[] IconsState;

	private PlayerInfo m_player;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!MasterManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		TextIcons = new UILabel[Icons.Length];
		for (int i = 0; i < Icons.Length; ++i)
			TextIcons[i] = Icons[i].GetComponentInChildren<UILabel>();
	}
	
	// Update is called once per frame
	public void UpdateUI (Item item) 
	{
		if (item != null && item.Icon != null)
		{
			// title
			Title.text = item.Name;
			// description
			Description.text = item.Description;
			// next level
			for(int i = 0; i < Icons.Length; ++i)
			{
				Icons[i].sprite2D = IconsState[(int)item.stats[i].type];
				TextIcons[i].text = "+" + item.stats[i].First + "%";
			}

			Gold.text = "sell > " + item.Price + " g"; 
		}
		else
		{
			Title.text = "vide";
			// description
			Description.text = "vide";
			// next level
			for(int i = 0; i < Icons.Length; ++i)
			{
				//Icons[i].sprite2D = ;
				TextIcons[i].text = "+0%";
			}

			Gold.text = "sell > 0 g";
		}
	}
}
