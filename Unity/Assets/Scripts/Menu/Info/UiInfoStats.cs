﻿using UnityEngine;
using System.Collections;

public class UiInfoStats : MonoBehaviour 
{
	public UILabel Description;
	public UILabel Title;
	public UI2DSprite Icon;
	private UILabel TextIcon;
	public UILabel Gold;

	private PlayerInfo m_player;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!MasterManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		TextIcon = Icon.GetComponentInChildren<UILabel>();
	}
	
	// Update is called once per frame
	public void UpdateUI (PlayerStat ps) 
	{
		// TODO : get the current icon to have the stat
		if (ps != null)
		{
			// title
			Title.text = "info " + ps.stat.type.ToString();
			// description
			Description.text = ps.Description;
			// next level
			TextIcon.text = "+" + ps.GetStatValue() + "% -> +" + ps.GetStatNextValue() + "% next lvl";

			Gold.text = "buy > " + ps.Price + " G";
		}
		else
		{
			Title.text = "info > 0 G";
			// description
			Description.text = "vide";
			// next level
			TextIcon.text = "+0% -> +0% next lvl";

			Gold.text = "buy > 0";
		}
	}
}
