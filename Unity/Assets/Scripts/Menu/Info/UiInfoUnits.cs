﻿using UnityEngine;
using System.Collections;

public class UiInfoUnits: MonoBehaviour 
{
	// Stats
	public UILabel ATK;
	public UILabel CRIT;
	public UILabel COST;
	public UILabel DIST;
	public UILabel HP;
	public UILabel VISION;

	// Text
	public UILabel Type;
	public UILabel Title;
	public UILabel Gold;

	private PlayerInfo m_player;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!MasterManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();
	}
	
	// Update is called once per frame
	public void UpdateUI (Unit unit) 
	{
		if (unit != null)
		{
			// title
			Title.text = unit.Name;
			// type
			Type.text = unit.typeUnit.ToString();
			//stats
			ATK.text = unit.GetStatValue(DataBase.TypeStats.DAMAGE_CAC).ToString();
			CRIT.text = unit.GetStatValue(DataBase.TypeStats.CRIT).ToString();
			COST.text = unit.GetStatValue(DataBase.TypeStats.COST).ToString();
			DIST.text = unit.GetStatValue(DataBase.TypeStats.DAMAGE_DIST).ToString();
			HP.text = unit.GetStatValue(DataBase.TypeStats.HP).ToString();
			VISION.text = unit.GetStatValue(DataBase.TypeStats.VISION).ToString();

			Gold.text = "buy > " + unit.Price + " g";
		}
		else
		{
			Title.text = "vide";
			// type
			Type.text = "none";
			//stats
			ATK.text = "0";
			CRIT.text = "0";
			COST.text = "0";
			DIST.text = "0";
			HP.text = "0";
			VISION.text = "0";

			Gold.text = "buy > 0 g";
		}
	}
}
