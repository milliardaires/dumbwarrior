﻿using UnityEngine;
using System.Collections;

public class DesactiveMe : MonoBehaviour 
{
	public void Disable()
	{
		gameObject.SetActive(false);
	}
}
