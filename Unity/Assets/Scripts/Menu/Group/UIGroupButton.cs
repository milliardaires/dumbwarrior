using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UIGroupButton : MonoBehaviour 
{
	// info
	public int player;
	protected int m_playerId;
	public int Columns;
	public bool Active = false;

	// transition
	public UIGroupButton NextGroup;
	public UIGroupButton BackGroup;

	// list of buttons
	protected UiIButton[] m_buttons;
	protected List<UiIButton> m_masters;
	protected int m_index = 0;

	public UiIButton Current
	{
		get {return m_buttons[m_index];}
	}

	// UI
	public UIPanel m_panel;
	public UiInfoPlayer InfoPlayer;

	protected PlayerInfo m_player;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!MasterManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

#if UNITY_EDITOR
		if (InfoManager.Instance.CountPlayerPlaying() == 0)
		{
			InfoManager.Instance.ActivePlayers(0);
			InfoManager.Instance.ActivePlayers(1);
		}
#endif
		m_masters = new List<UiIButton>();
		m_playerId = InfoManager.Instance.GetIdPlayer(player);
		m_player = InfoManager.Instance.GetPlayer(player);

		Init();

		yield return new WaitForEndOfFrame();

		if (Active)
			Current.SendMessage("OnSelect", true, SendMessageOptions.RequireReceiver);
	}

	public bool MasterFill()
	{
		foreach(UiIButton unit in m_masters)
			if (unit.Fill)
				return true;

		return false;
	}

#region Init
	protected virtual void Init()
	{
		// get buttons
		m_buttons = GetComponentsInChildren<UiIButton>();
		m_buttons = m_buttons.Select( x => x).OrderBy(x => x.name).ToArray();
		//Debug.Log("init group button");
	}

	public IEnumerator Show(bool up)
	{
		yield return new WaitForEndOfFrame();

		Active = true;
		m_panel.depth = 0;
		if (up)
			m_index = 0;
		else
			m_index = m_buttons.Length - 1;
		Current.SendMessage("OnSelect", true, SendMessageOptions.RequireReceiver);

	}

	protected void AddSprite(Sprite sprite)
	{
		foreach(UiIButton unit in m_masters)
		{
			if (!unit.Fill)
			{
				unit.AddSprite(sprite);
				break;
			}
		}
	}
#endregion

	// Update is called once per frame
	void Update () 
	{
		if (Active)
			UpdateInput();
	}

	void UpdateInput()
	{
		if(InputManager.Instance[m_playerId].PressA())
		{
			// onclick current
			Current.SendMessage("OnClick", SendMessageOptions.RequireReceiver);
		}
		else if(InputManager.Instance[m_playerId].PressB())
		{
			// sell
			Current.Action2();
		}
		else if(InputManager.Instance[m_playerId].PressLeft())
		{
			BackButton();
		}
		else if(InputManager.Instance[m_playerId].PressRight())
		{
			NextButton();
		}
		else if(InputManager.Instance[m_playerId].PressUp())
		{
			// backup
			BackUpButton();
		}
		else if(InputManager.Instance[m_playerId].PressDown())
		{
			// nextdown
			NextDownButton();
		}
	}

	private void NextButton()
	{
		int index = m_index + 1;
		if (index >= m_buttons.Length)
			index = 0;

		StartCoroutine(CallResetFunc(index, "OnSelect"));
	}

	private void BackButton()
	{
		int index = m_index - 1;
		if (index < 0)
			index = m_buttons.Length - 1;
		
		StartCoroutine(CallResetFunc(index, "OnSelect"));
	}

	private void NextDownButton()
	{
		int index = m_index + Columns;
		if (index < m_buttons.Length)
		{
			StartCoroutine(CallResetFunc(index, "OnSelect"));
		}
		else
		{
			// TODO : go to the next group
			Disable();
			StartCoroutine(NextGroup.Show(true));
		}
	}

	private void BackUpButton()
	{
		int index = m_index - Columns;
		if (index >= 0)
		{
			StartCoroutine(CallResetFunc(index, "OnSelect"));
		}
		else
		{
			// TODO : go to the next group
			Disable();
			StartCoroutine(BackGroup.Show(false));
		}
	}

	void Disable()
	{
		Active = false;
		Current.SendMessage("OnSelect", false, SendMessageOptions.RequireReceiver);
		m_panel.depth = -1;
	}

	#region Notify
	public IEnumerator CallResetFunc(int index, string function)
	{
		Current.SendMessage(function, false, SendMessageOptions.RequireReceiver);
		
		yield return new WaitForEndOfFrame();
		m_index = index;
		
		Current.SendMessage(function, true, SendMessageOptions.RequireReceiver);
	}
	#endregion
}
