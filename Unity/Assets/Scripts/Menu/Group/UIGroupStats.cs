﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UIGroupStats : UIGroupButton 
{
	// UI
	public UiInfoStats Info;

#region Init
	protected override void Init()
	{
		// get buttons
		m_buttons = GetComponentsInChildren<UiIButton>();
		m_buttons = m_buttons.Select( x => x).OrderBy(x => x.name).ToArray();

		int i = 0;
		foreach(UiIButton b in m_buttons)
		{
			UIStats stat = b.GetComponent<UIStats>();		
			if (i < m_player.Stats.Count)
			{
				stat.Init(Info, m_player.Stats[i]);
				++i;
			}
			else
				stat.Init(Info, null);
			stat.OnClic += BuyLevel;

		}
	}

	void BuyLevel(PlayerStat stat)
	{
		m_player.BuyLevelUp(stat);
		InfoPlayer.Refresh();
	}
#endregion
}
