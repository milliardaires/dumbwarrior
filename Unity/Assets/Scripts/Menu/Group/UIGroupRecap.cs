﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UIGroupRecap : UIGroupButton 
{
	// UI
	public UiInfoStats Info;

#region Init
	protected override void Init()
	{
		base.Init();

		int i = 0;
		for(; i < 6; ++i)
		{
			if (i < m_player.CountUnit)
			{
				m_buttons[i].AddSprite(m_player.GetCurrentUnit(i).SpriteUI);
			}
		}

		for(int j = 0; i < m_buttons.Length; ++i, ++j)
		{
			if (j < m_player.CurrentItems.Count)
			{
				m_buttons[i].AddSprite(m_player.GetCurrentItem(j).Icon);
			}
		}
	}
#endregion
}
