﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UIGroupItems : UIGroupButton 
{
	// UI
	public UiInfoItems Info;

#region Init
	protected override void Init()
	{
		// get buttons
		m_buttons = GetComponentsInChildren<UiIButton>();
		m_buttons = m_buttons.Select( x => x).OrderBy(x => x.name).ToArray();
		
		int i = 0;
		int j = 0;
		foreach(UiIButton b in m_buttons)
		{
			UIItems items = b.GetComponent<UIItems>();
			Item item = null;

			if (!b.Master)
			{
				if (i < m_player.Items.Count)
				{
					item = m_player.Items[i];
					++i;
				}
				items.OnSell += SellItem;
			}
			else
			{
				if (j < m_player.CurrentItems.Count)
				{
					item = m_player.CurrentItems[j];
					items.Fill = true;
					++j;
				}
				m_masters.Add(b);
			}

			items.Init(Info, item);
			items.OnClic += AddItem;
		}
	}
#endregion

	void SellItem(Item item)
	{
		m_player.SellItem(item);
		InfoPlayer.Refresh();

		foreach(UiIButton b in m_masters)
		{
			if (b.Fill)
			{
				UIItems items = b.GetComponent<UIItems>();
				if(items.ItemName == item.Name)
				{
					items.RemoveItem();
					m_player.RemoveCurrentItem(item);
				}
			}
		}
	}

	void AddItem(UIItems ui, Item item, bool add)
	{
		if (add)
		{
			if (item.Container)
			{
				Item newItem = ItemsManager.Instance.GetItem(item.category);
				m_player.OpenContainer(item, newItem);
				ui.ReplaceItem(newItem);
			}
			else
			{
				foreach(UiIButton b in m_masters)
				{
					if (!b.Fill)
					{
						UIItems items = b.GetComponent<UIItems>();
						items.AddOnMaster(item);
						m_player.AddCurrentItem(item);
						break;
					}
				}
			}
		}
		else
		{
			m_player.RemoveCurrentItem(item);
		}
	}
}
