﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UIGroupUnits : UIGroupButton 
{
	// UI
	public UiInfoUnits Info;

#region Init
	protected override void Init()
	{
		// get buttons
		m_buttons = GetComponentsInChildren<UiIButton>();
		m_buttons = m_buttons.Select( x => x).OrderBy(x => x.name).ToArray();
		
		int i = 0;
		int j = 0;
		bool first = false;
		foreach(UiIButton b in m_buttons)
		{		
			UIUnits uiUnit = b.GetComponent<UIUnits>();
			Unit unit = null;

			if (!b.Master)
			{
				if (i < m_player.Units.Count)
				{
					unit = m_player.Units[i];
					++i;
				}
				uiUnit.OnBuy += BuyUnit;
			}
			else
			{
				if (j < m_player.CountUnit)
				{
					if (j == 0)
					{
						first = true;
					}
					uiUnit.Fill = true;
					unit = m_player.GetCurrentUnit(j);
					++j;
				}
				m_masters.Add(b);
			}

			uiUnit.Init(Info, unit);
			if (first)
				first = false;
			else
				uiUnit.OnClic += AddUnit;
		}
	}
#endregion

	void BuyUnit (Unit unit)
	{
		m_player.BuyUnit(unit);
		InfoPlayer.Refresh();
	}

	void AddUnit(Unit unit, bool add)
	{
		if (add)
		{
			foreach(UiIButton b in m_masters)
			{
				if (!b.Fill)
				{
					UIUnits units = b.GetComponent<UIUnits>();
					if (m_player.AddCurrentUnit(unit))
						units.AddOnMaster(unit);
					break;
				}
			}
		}
		else
		{
			m_player.RemoveCurrentUnit(unit);
		}
	}
}
