﻿using UnityEngine;
using System.Collections;

public class TestInput : MonoBehaviour 
{
	[Range(0,4)]
	public int player = 0;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(InputManager.Instance[player].PressA())
		{
			Debug.Log("press A");
		}
		else if(InputManager.Instance[player].PressB())
		{
			Debug.Log("press B");
		}
		else if(InputManager.Instance[player].PressStart())
		{
			Debug.Log("press Start");
		}
		else if(InputManager.Instance[player].PressLeft())
		{
			Debug.Log("press left");
		}
		else if(InputManager.Instance[player].PressRight())
		{
			Debug.Log("press right");
		}
		else if(InputManager.Instance[player].PressUp())
		{
			Debug.Log("press up");
		}
		else if(InputManager.Instance[player].PressDown())
		{
			Debug.Log("press down");
		}
	}
}
