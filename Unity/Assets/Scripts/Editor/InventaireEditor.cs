﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Inventaire))]
public class InventaireEditor : Editor
{
	private Inventaire inv;
	//private List<Texture2D> m_preview = new List<Texture2D>();
	private string atlas = "S_Objects";
	public Color[] ColorCat;

	void OnEnable () 
	{
		inv = (Inventaire)target;
		ColorCat = new Color[]{Color.white, Color.gray, Color.yellow, Color.red, new Color(1, 0, 1, 1)};
	}

	public override void OnInspectorGUI () 
	{
		GUI.changed = false;

		// Intro
		GUILayout.Label("Iventaire", EditorStyles.boldLabel);
		GUILayout.Label("Count Items : " + inv.Items.Count);
		EditorGUILayout.BeginHorizontal("GroupBox");
		{
			inv.MatItem = EditorGUILayout.ObjectField(inv.MatItem, typeof(Material), false) as Material;
			if (GUILayout.Button("Update Coffre"))
			{
				inv.FillContainers();
			}
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal("GroupBox");
		{
			atlas = EditorGUILayout.TextArea(atlas);
			if (GUILayout.Button("auto"))
			{
				inv.CreateAuto(atlas);
			}
			if (GUILayout.Button("clean"))
			{
				if (EditorUtility.DisplayDialog("Secure", "C bon mec ?", "Yes", "No"))
					inv.ResetList();
			}
			// Button
			if (GUILayout.Button("New Item"))
			{
				inv.Items.Add(new Item());
			}
		}
		EditorGUILayout.EndHorizontal();

		// Items
		for(int i = 0; i < inv.Items.Count; ++i)
		{
			Item item = inv.Items[i];
			GUILayout.BeginHorizontal();
			GUI.contentColor = ColorCat[(int)item.category];
			// FOLDOUT
			if (GUILayout.Button(item.Name, "OL Titleleft"))
				item.show = !item.show;
			// ADD
			if (GUILayout.Button("+", "OL Titlemid"))
				inv.Items.Add(new Item());
			// REMOVE
			if (GUILayout.Button("-", "OL Titleright"))
				if (EditorUtility.DisplayDialog("Non sérieux?", "Are you sure you want to delete " + item.Name + "  ?", "Yes", "No"))
					inv.Items.Remove(item);
			GUI.contentColor = Color.white;
			GUILayout.EndHorizontal();

			// Item
			if (item.show)
			{
				GUILayout.BeginHorizontal();
				{
					// SPRITE
					GUILayout.BeginVertical();
					item.Icon = EditorGUILayout.ObjectField(item.Icon, typeof(Sprite), false, GUILayout.Width(100)) as Sprite;
					// TODO : preview sprite
					if (item.Icon != null)
					{
						Texture2D tex = item.Icon.texture;
						Rect preview = EditorGUILayout.GetControlRect(GUILayout.MaxWidth(20));
						Color[] colors = tex.GetPixels((int)item.Icon.textureRect.x, (int)item.Icon.textureRect.y, (int)item.Icon.textureRect.width, (int)item.Icon.textureRect.height);
						Texture2D previewSprite = new Texture2D((int)item.Icon.textureRect.width, (int)item.Icon.textureRect.height);
						previewSprite.hideFlags = HideFlags.HideAndDontSave;
						previewSprite.SetPixels(colors);
						previewSprite.Apply();
						preview.width = previewSprite.width * 2f;
						preview.height = previewSprite.height * 2f;
						EditorGUI.DrawPreviewTexture(preview, previewSprite, inv.MatItem);
					}

					foreach(Item check in inv.Items)
					{
						if (check != item && check.Icon == item.Icon)
						{
							GUI.color = Color.red;
							GUILayout.Label("Sprite Already Use");
							GUI.color = Color.white;
							break;
						}
					}
					GUILayout.EndVertical();

					// EFFECT
					GUILayout.BeginVertical();
					{
						// NAME
						EditorGUILayout.BeginHorizontal();
						{
							//EditorGUILayout.PrefixLabel("Name");
							item.Name = EditorGUILayout.TextField(item.Name, GUILayout.Width(150));
							EditorGUILayout.LabelField("Price (Gold)", GUILayout.Width(80));
							item.Price = EditorGUILayout.IntField(item.Price, GUILayout.Width(50));
						}
						EditorGUILayout.EndHorizontal();

						// Stats
						foreach(Stats stats in item.stats)
						{
							EditorGUILayout.BeginHorizontal();
							stats.type = (DataBase.TypeStats)EditorGUILayout.EnumPopup(stats.type);
							stats.First = EditorGUILayout.IntField(stats.First);
							EditorGUILayout.EndHorizontal();
						}
						EditorGUILayout.BeginHorizontal();
						{
							item.category = (DataBase.Category)EditorGUILayout.EnumPopup(item.category);
							EditorGUILayout.LabelField("Container", GUILayout.Width(60));
							item.Container = EditorGUILayout.Toggle(item.Container, GUILayout.Width(15));
							if (item.Container)
							{
								EditorGUILayout.LabelField("Rate %", GUILayout.Width(50));
								item.Rate = EditorGUILayout.IntField(item.Rate, GUILayout.Width(50));
							}
						}
						EditorGUILayout.EndHorizontal();
					}
					GUILayout.EndVertical();
				}
				GUILayout.EndHorizontal();

				EditorGUILayout.LabelField("Description");
				item.Description = EditorGUILayout.TextArea(item.Description);
			}
		}

		if(GUI.changed)
		{
			EditorUtility.SetDirty(inv);
			
		}

	}
}
