﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(SystemUnit))]
public class SystemUnitEditor : Editor 
{
	private SystemUnit m_target;
	private string NameUnit;

	void OnEnable()
	{
		m_target = (SystemUnit) target;
	}

	public override void OnInspectorGUI()
	{
		//serializedObject.Update() ;		
		GUI.changed = false;

		EditorGUILayout.LabelField("System Unit");
		EditorGUILayout.BeginHorizontal("GroupBox");
		{			
			EditorGUILayout.LabelField("Max Level");
			m_target.LevelMax = EditorGUILayout.IntSlider(m_target.LevelMax, 1, 10);

		}
		EditorGUILayout.EndHorizontal();
		m_target.MatItem = EditorGUILayout.ObjectField(m_target.MatItem, typeof(Material), false) as Material;
		for( int j = 0; j < m_target.Units.Count; ++j)
		{
			Unit unit = m_target.Units[j];
			EditorGUILayout.BeginVertical("GroupBox");
			{
				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button(unit.Name, "OL Titleleft"))
						unit.show = !unit.show;

					// REMOVE
					if (GUILayout.Button("-", "OL Titleright"))
						if (EditorUtility.DisplayDialog("wtf?", "supprimer " + unit.Name + "  ?", "Yes", "No"))
							m_target.Units.Remove(unit);
				}
				EditorGUILayout.EndHorizontal();
				if (unit.show)
				{
					EditorGUILayout.BeginHorizontal();
					{
						unit.typeUnit = (DataBase.TypeUnits)EditorGUILayout.EnumPopup(unit.typeUnit);

						unit.SpriteUI = (Sprite)EditorGUILayout.ObjectField(unit.SpriteUI, typeof(Sprite), false);
						// ADD SKILL
						//EditorGUILayout.LabelField("Skills");
						if (GUILayout.Button("Add Skill"))
							unit.stats.Add(new Stats(m_target.LevelMax));
					}
					EditorGUILayout.EndHorizontal();
					EditorGUILayout.BeginHorizontal();
					{
						if (unit.SpriteUI != null)
						{
							Texture2D tex = unit.SpriteUI.texture;
							Rect preview = EditorGUILayout.GetControlRect(GUILayout.Width(150), GUILayout.Height(25));
							Color[] colors = tex.GetPixels((int)unit.SpriteUI.textureRect.x, (int)unit.SpriteUI.textureRect.y, (int)unit.SpriteUI.textureRect.width, (int)unit.SpriteUI.textureRect.height);
							Texture2D previewSprite = new Texture2D((int)unit.SpriteUI.textureRect.width, (int)unit.SpriteUI.textureRect.height);
							previewSprite.hideFlags = HideFlags.HideAndDontSave;
							previewSprite.SetPixels(colors);
							previewSprite.Apply();
							preview.width = previewSprite.width * 2f;
							preview.height = previewSprite.height * 2f;
							EditorGUI.DrawPreviewTexture(preview, previewSprite, m_target.MatItem);
						}
						EditorGUILayout.LabelField("Available", GUILayout.Width(60));
						unit.Available = EditorGUILayout.Toggle(unit.Available, GUILayout.Width(50));
						EditorGUILayout.LabelField("Price (Gold)", GUILayout.Width(80));
						unit.Price = EditorGUILayout.IntField(unit.Price, GUILayout.Width(50));
					}
					EditorGUILayout.EndHorizontal();
					// SKILLS
					for (int k = 0; k < unit.stats.Count; ++k)
					{
						Stats skill = unit.stats[k];
						EditorGUILayout.BeginVertical("GroupBox");
						{
							EditorGUILayout.BeginHorizontal();
							{
								if (GUILayout.Button(skill.type.ToString(), "OL Titleleft"))
									skill.show = !skill.show;

								// REMOVE
								if (GUILayout.Button("-", "OL Titleright"))
									if (EditorUtility.DisplayDialog("Encore", "supprimer " + skill.type + "  ?", "Yes", "No"))
										unit.stats.Remove(skill);
							}
							EditorGUILayout.EndHorizontal();
							if (skill.show)
							{
								EditorGUILayout.BeginHorizontal();
								{
									// skill
									skill.type = (DataBase.TypeStats)EditorGUILayout.EnumPopup(skill.type);
									skill.Max = EditorGUILayout.IntField("Max " + skill.type.ToString(), skill.Max);
								}
								EditorGUILayout.EndHorizontal();
								for (int i = 0; i < skill.value.Count; ++i)
								{
									// LEVEL
									skill.value[i] = EditorGUILayout.IntSlider(skill.value[i], 1, skill.Max);
									Rect bar = GUILayoutUtility.GetRect(0, 20);
									EditorGUI.ProgressBar(bar, skill.value[i] / (float)skill.Max, "Level " + (i + 1));
								}
							}
						}
						EditorGUILayout.EndVertical();
					}
				}
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.BeginHorizontal("GroupBox");
		{
			// ADD
			NameUnit = EditorGUILayout.TextField(NameUnit);
			if (GUILayout.Button("Add"))
			{
				m_target.Units.Add(new Unit(NameUnit, m_target.LevelMax));
			}
//			if (GUILayout.Button("Update"))
//			{
//				m_target.FillDico();
//			}
		}
		EditorGUILayout.EndHorizontal();

		//serializedObject.ApplyModifiedProperties() ;			
		if (GUI.changed)
        	EditorUtility.SetDirty(m_target);
	}
}