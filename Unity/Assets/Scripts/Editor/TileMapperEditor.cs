﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[CustomEditor(typeof(TileMapper))]
public class TileMapperEditor : Editor 
{
	// TEXTURE
	SerializedProperty SizeTileX;
	SerializedProperty SizeTileY;
	SerializedProperty HeightMap;
	SerializedProperty ListMaps;
	Environnement BiomEnv;

	TileMapper mapper;
	bool visible;
	//int idTypeBiom;

	// DICO GUI
	int toRemove;

	// Use this for initialization
	void OnEnable () 
	{
		mapper = (TileMapper)target;

		SizeTileX = serializedObject.FindProperty("SizeTileX");
		SizeTileY = serializedObject.FindProperty("SizeTileY");
		HeightMap = serializedObject.FindProperty("HeightMap");
		ListMaps = serializedObject.FindProperty("ListMaps");
	}
	
	// Update is called once per frame
	public override void OnInspectorGUI () 
	{
		serializedObject.Update ();

		GUI.changed = false;

		EditorGUILayout.PropertyField(SizeTileX, new GUIContent("Size of Tile X"));
		EditorGUILayout.PropertyField(SizeTileY, new GUIContent("Size of Tile Y"));
		EditorGUILayout.PropertyField(HeightMap, new GUIContent("HeightMap"));
		mapper.BiomEnv = EditorGUILayout.ObjectField("Biom", mapper.BiomEnv, typeof(Environnement), false) as Environnement;
		// POPUP BIOM
		mapper.idTypeBiom = EditorGUILayout.Popup(mapper.idTypeBiom, mapper.BiomEnv.TypeOfBiom.ToArray());
		EditorGUIUtility.LookLikeControls();
		EditorGUILayout.PropertyField(ListMaps, new GUIContent("ListMaps"), true);
		

		// CREATE
		if (GUILayout.Button("Create"))
		{
			mapper.Init();
			mapper.BuildMap();
		}

		// CLEAN
		if (GUILayout.Button("ClearMap"))
		{
			mapper.ClearMap();
		}

		if(GUI.changed)
		{
			EditorUtility.SetDirty(mapper);
			
		}
		serializedObject.ApplyModifiedProperties ();
	}
}
