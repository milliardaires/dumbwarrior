﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[CustomEditor(typeof(Environnement))]
public class EnvironnementEditor : Editor
{
	private Environnement env;
	private string typeBiom, typeTile;
	private int idTypeBiom;
	private Sprite biomSprite;
	private Texture2D AtlasBiom;

	void OnEnable () 
	{
		env = (Environnement)target;
		biomSprite = null;
	}
	
	public override void OnInspectorGUI () 
	{
		GUI.changed = false;
		
		// Intro
		GUILayout.Label("Environnement", EditorStyles.boldLabel);
		env.MatTile = EditorGUILayout.ObjectField(env.MatTile, typeof(Material), false) as Material;

		UITypeBiom();

		GUILayout.BeginVertical("GroupBox");
		EditorGUILayout.BeginHorizontal();
		typeTile = EditorGUILayout.TextField(typeTile);
		if (GUILayout.Button("New Type Tile") && typeTile != string.Empty)
		{
			env.TypeOfTile.Add(new TileBioms(typeTile));
			typeTile = string.Empty;
		}
		EditorGUILayout.EndHorizontal();
		GUILayout.Label("TILES");
		// Items
		for(int i = 0; i < env.TypeOfTile.Count; ++i)
		{
			GUILayout.BeginVertical("GroupBox");
			TileBioms item = env.TypeOfTile[i];
			GUILayout.Label("Biom : " + item.Bioms.Count + " [RGB " + (item.PixelColor.r * 255) + " " + (item.PixelColor.g * 255) + " " + (item.PixelColor.b * 255) + "]");
			GUILayout.BeginHorizontal();
			// FOLDOUT
			GUI.contentColor = item.PixelColor;
			if (GUILayout.Button("Type " + item.TypeTile, "OL Titleleft"))
			{
				item.show = !item.show;
			}
			GUI.contentColor = Color.white;
			// REMOVE
			if (GUILayout.Button("-", "OL Titleright"))
				if (EditorUtility.DisplayDialog("Window anti newbie", "Are you sure you want to delete " + item.TypeTile + "  ?", "Yes", "No"))
					env.TypeOfTile.Remove(item);
			GUILayout.EndHorizontal();
			
			// Item
			if (item.show)
			{
				GUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Pixel Color");
				item.PixelColor = EditorGUILayout.ColorField(item.PixelColor, GUILayout.MaxWidth(100));
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
				// POPUP BIOM
				idTypeBiom = EditorGUILayout.Popup(idTypeBiom, env.TypeOfBiom.ToArray());
				if (GUILayout.Button("Add Biom"))
				{
					//Debug.Log(idTypeBiom + " " + env.TypeOfBiom[idTypeBiom]);
					item.Bioms.Add(new Biom(env.TypeOfBiom[idTypeBiom]));
				}
				GUILayout.EndHorizontal();

				for(int j = 0; j < item.Bioms.Count; ++j)
				{
					Biom biom = item.Bioms[j];
					GUILayout.BeginVertical("GroupBox");
					// NAME
					EditorGUILayout.BeginHorizontal();
						GUI.contentColor = item.PixelColor;
						if (GUILayout.Button("Biom " + biom.TypeBiom, "OL Titleleft"))
						{
							biom.show = !biom.show;
						}
						GUI.contentColor = Color.white;
						if (GUILayout.Button("-", "OL Titleright"))
							if (EditorUtility.DisplayDialog("Newin le miss click", "Are you sure you want to delete " + biom + "  ?", "Yes", "No"))
							item.Bioms.Remove(biom);
					EditorGUILayout.EndHorizontal();

					if (biom.show)
					{
						// Sprite
						EditorGUILayout.BeginHorizontal();
						biomSprite = EditorGUILayout.ObjectField(biomSprite, typeof(Sprite), false) as Sprite;
						if (GUILayout.Button("Add Tile") && biomSprite != null)
						{
							bool canCreate = true;
							foreach(BiomSprite bs in biom.Sprites)
							{
								if (bs.Tile == biomSprite)
								{
									canCreate = false;
								}
							}
							if (canCreate)
								biom.Sprites.Add(new BiomSprite(biomSprite));
						}
						EditorGUILayout.EndHorizontal();

						for(int k = 0; k < biom.Sprites.Count; ++k)
						{
							BiomSprite bSprite = biom.Sprites[k];
							EditorGUILayout.BeginVertical("GroupBox");
							EditorGUILayout.LabelField(bSprite.Tile.name);
							// Sprite
							EditorGUILayout.BeginHorizontal();
							// TODO : preview sprite
							if (bSprite.Tile != null)
							{
								Texture2D tex = bSprite.Tile.texture;
								Color[] colors = tex.GetPixels((int)bSprite.Tile.textureRect.x, (int)bSprite.Tile.textureRect.y, (int)bSprite.Tile.textureRect.width, (int)bSprite.Tile.textureRect.height);
								Texture2D previewSprite = new Texture2D((int)bSprite.Tile.textureRect.width, (int)bSprite.Tile.textureRect.height);
								previewSprite.hideFlags = HideFlags.HideAndDontSave;
								Rect preview = EditorGUILayout.GetControlRect(GUILayout.Width(previewSprite.width * 2f), GUILayout.Height(previewSprite.height * 2f));
								previewSprite.SetPixels(colors);
								previewSprite.Apply();
								preview.width = previewSprite.width * 2f;
								preview.height = previewSprite.height * 2f;
								EditorGUI.DrawPreviewTexture(preview, previewSprite, env.MatTile);
							}
							// INFO
							EditorGUILayout.BeginVertical();
							// POURCENT
							EditorGUILayout.BeginHorizontal();
							bSprite.Pourcent = EditorGUILayout.IntField(bSprite.Pourcent, GUILayout.MaxWidth(30));
							GUILayout.Label("%");
							if (GUILayout.Button("x", GUILayout.MaxWidth(30)))
								biom.Sprites.Remove(bSprite);
							EditorGUILayout.EndHorizontal();
							// WALKABLE
							EditorGUILayout.BeginHorizontal();
							GUILayout.Label("Walkable");
							bSprite.Walkable = EditorGUILayout.Toggle(bSprite.Walkable, GUILayout.MaxWidth(30));
							EditorGUILayout.EndHorizontal();
							// ZORDER
							EditorGUILayout.BeginHorizontal();
							GUILayout.Label("Depth");
							bSprite.Zorder = EditorGUILayout.IntField(bSprite.Zorder, GUILayout.MaxWidth(30));
							EditorGUILayout.EndHorizontal();

							// MATERIAL
							EditorGUILayout.BeginHorizontal();
							GUILayout.Label("Mat diff");
							bSprite.Special = EditorGUILayout.ObjectField(bSprite.Special,typeof(Material), false) as Material;
							EditorGUILayout.EndHorizontal();

							EditorGUILayout.EndVertical();

							EditorGUILayout.EndHorizontal();
							EditorGUILayout.EndVertical();
						}
					}
					GUILayout.EndVertical();
				}
			}
			GUILayout.EndVertical();
		}
		GUILayout.EndVertical();

		if(GUI.changed)
		{
			EditorUtility.SetDirty(env);			
		}
		
	}

	void UITypeBiom()
	{
		// Add Type Biom
		GUILayout.BeginVertical("GroupBox");
		EditorGUILayout.BeginHorizontal();
		typeBiom = EditorGUILayout.TextField(typeBiom);
		AtlasBiom = EditorGUILayout.ObjectField(AtlasBiom, typeof(Texture2D), false) as Texture2D;
		if (GUILayout.Button("New Type Biom") && typeBiom != string.Empty)
		{
			if (AtlasBiom != null)
				env.CreateAuto(typeBiom, AtlasBiom.name);
			else
				env.TypeOfBiom.Add(typeBiom);
			typeBiom = string.Empty;
			AtlasBiom = null;
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginVertical();
		GUILayout.Label("BIOMS");
		// LIST BIOM
		for(int i = 0; i < env.TypeOfBiom.Count; ++i)
		{
			string biom = env.TypeOfBiom[i];
			GUILayout.BeginHorizontal();
			GUI.contentColor = Color.green;
			GUILayout.Label(biom, "OL Titleleft");
			GUI.contentColor = Color.white;
			// REMOVE
			if (GUILayout.Button("-", "OL Titleright"))
				if (EditorUtility.DisplayDialog("Newin le noob?", "Are you sure you want to delete " + biom + "  ?", "Yes", "No"))
					env.RemoveBiom(biom);
			GUILayout.EndHorizontal();
		}
		EditorGUILayout.EndVertical();
		GUILayout.EndVertical();
	}
}
