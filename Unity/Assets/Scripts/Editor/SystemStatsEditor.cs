﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(SystemStats))]
public class SystemStatsEditor : Editor 
{
	private SystemStats m_target;

	void OnEnable()
	{
		m_target = (SystemStats) target;
	}

	public override void OnInspectorGUI()
	{	
		GUI.changed = false;

		EditorGUILayout.LabelField("System Stats for Player");
		EditorGUILayout.BeginHorizontal("GroupBox");
		{			
			EditorGUILayout.LabelField("Max Level");
			m_target.LevelMax = EditorGUILayout.IntSlider(m_target.LevelMax, 1, 10);
		}
		EditorGUILayout.EndHorizontal();

		for( int j = 0; j < m_target.Stats.Count; ++j)
		{
			PlayerStat ps = m_target.Stats[j];
			Stats stat = ps.stat;

			EditorGUILayout.BeginVertical("GroupBox");
			{
				EditorGUILayout.BeginHorizontal();
				{
					if (GUILayout.Button(stat.type.ToString(), "OL Titleleft"))
						ps.show = !ps.show;

					// REMOVE
					if (GUILayout.Button("-", "OL Titleright"))
						if (EditorUtility.DisplayDialog("wtf?", "supprimer " + stat.type.ToString() + "  ?", "Yes", "No"))
							m_target.Stats.Remove(ps);
				}
				EditorGUILayout.EndHorizontal();

				if (ps.show)
				{
					EditorGUILayout.BeginHorizontal();
					{
						stat.type = (DataBase.TypeStats)EditorGUILayout.EnumPopup(stat.type);
						stat.Max = EditorGUILayout.IntField("Max " + stat.type.ToString(), stat.Max);
					}
					EditorGUILayout.EndHorizontal();

					for (int i = 0; i < stat.value.Count; ++i)
					{													
						stat.value[i] = EditorGUILayout.IntSlider(stat.value[i], 1, stat.Max);
						Rect bar = GUILayoutUtility.GetRect(0, 20);
						EditorGUI.ProgressBar(bar, stat.value[i] / (float)stat.Max, "Level " + (i + 1));							
					}
					EditorGUILayout.BeginHorizontal();
					{
						EditorGUILayout.LabelField("Cost level up (Gold)");
						ps.Price = EditorGUILayout.IntField(ps.Price, GUILayout.Width(50));
					}
					EditorGUILayout.EndHorizontal();
				}
			}
			EditorGUILayout.LabelField("Description");
			ps.Description = EditorGUILayout.TextArea(ps.Description);
			EditorGUILayout.EndVertical();
		}
		EditorGUILayout.BeginHorizontal("GroupBox");
		{
			// ADD
			if (GUILayout.Button("Add Stat"))
			{
				m_target.Stats.Add(new PlayerStat(m_target.LevelMax));
			}
			if (GUILayout.Button("Refresh Level"))
			{
				m_target.Refresh();
			}
		}
		EditorGUILayout.EndHorizontal();
		
		if (GUI.changed)
        	EditorUtility.SetDirty(m_target);
	}
}