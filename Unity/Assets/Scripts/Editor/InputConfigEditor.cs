﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(InputConfig))]
public class InputConfigEditor : Editor
{
	private InputConfig cfg;

	void OnEnable()
	{
		cfg = (InputConfig) target;
	}

	public override void OnInspectorGUI ()
	{
		GUI.changed = false;

		EditorGUILayout.BeginVertical();
		{
			GUILayout.Label("Input Config");
			int i = 1;
			foreach(PlayerInput player in cfg.Inputs)
			{
				EditorGUILayout.BeginVertical("GroupBox");
				GUILayout.Label("Player " + i);
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Label("Button A");
					player.ButtonA.Keyboard = (KeyCode)EditorGUILayout.EnumPopup(player.ButtonA.Keyboard);
					player.ButtonA.Controller = (KeyCode)EditorGUILayout.EnumPopup(player.ButtonA.Controller);
				}
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Label("Button B");
					player.ButtonB.Keyboard = (KeyCode)EditorGUILayout.EnumPopup(player.ButtonB.Keyboard);
					player.ButtonB.Controller = (KeyCode)EditorGUILayout.EnumPopup(player.ButtonB.Controller);
				}
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Label("Button Start");
					player.ButtonStart.Keyboard = (KeyCode)EditorGUILayout.EnumPopup(player.ButtonStart.Keyboard);
					player.ButtonStart.Controller = (KeyCode)EditorGUILayout.EnumPopup(player.ButtonStart.Controller);
				}
				EditorGUILayout.EndHorizontal();
				GUILayout.Label("Axis");
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Label("Left");
					player.Arrows.ButtonLeft.Keyboard = (KeyCode)EditorGUILayout.EnumPopup(player.Arrows.ButtonLeft.Keyboard);
					player.Arrows.ButtonLeft.Axis = EditorGUILayout.TextField(player.Arrows.ButtonLeft.Axis);
				}
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Label("Right");
					player.Arrows.ButtonRight.Keyboard = (KeyCode)EditorGUILayout.EnumPopup(player.Arrows.ButtonRight.Keyboard);
					player.Arrows.ButtonRight.Axis = EditorGUILayout.TextField(player.Arrows.ButtonRight.Axis);
				}
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Label("Up");
					player.Arrows.ButtonUp.Keyboard = (KeyCode)EditorGUILayout.EnumPopup(player.Arrows.ButtonUp.Keyboard);
					player.Arrows.ButtonUp.Axis = EditorGUILayout.TextField(player.Arrows.ButtonUp.Axis);
				}
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.BeginHorizontal();
				{
					GUILayout.Label("Down");
					player.Arrows.ButtonDown.Keyboard = (KeyCode)EditorGUILayout.EnumPopup(player.Arrows.ButtonDown.Keyboard);
					player.Arrows.ButtonDown.Axis = EditorGUILayout.TextField(player.Arrows.ButtonDown.Axis);
				}
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.EndVertical();
				++i;
			}
		}
		EditorGUILayout.EndVertical();

		if(GUI.changed)
		{
			EditorUtility.SetDirty(cfg);			
		}
	}
}
