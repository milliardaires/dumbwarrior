﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof (Pos2D))]
public class Pos2DDrawer : PropertyDrawer 
{
	public override void OnGUI (Rect pos, SerializedProperty prop, GUIContent label) 
	{
		SerializedProperty x = prop.FindPropertyRelative ("X");
		SerializedProperty y = prop.FindPropertyRelative ("Y");

		Rect rectX = pos;
		Rect rectY = pos;

		rectX.width /= 2;
		rectY.width /= 2;
		rectY.x += rectX.width;

		EditorGUI.PropertyField(rectX, x);
		EditorGUI.PropertyField(rectY, y);

	}
}
