﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CreateAsset
{
	[MenuItem("Assets/Create/Inventaire")]
	public static void CreateInv()
	{
		Inventaire inv = ScriptableObject.CreateInstance ("Inventaire") as Inventaire;
		AssetDatabase.CreateAsset(inv, "Assets/Config/Items.asset");
		AssetDatabase.SaveAssets();
	}

	[MenuItem("Assets/Create/Biom")]
	public static void CreateEnv()
	{
		Environnement env = ScriptableObject.CreateInstance ("Environnement") as Environnement;
		AssetDatabase.CreateAsset(env, "Assets/Config/Bioms.asset");
		AssetDatabase.SaveAssets();
	}

	[MenuItem("Assets/Create/Inputs")]
	public static void CreateInputs()
	{
		InputConfig env = ScriptableObject.CreateInstance ("InputConfig") as InputConfig;
		AssetDatabase.CreateAsset(env, "Assets/Config/Inputs.asset");
		AssetDatabase.SaveAssets();
	}

	[MenuItem("Assets/Create/SystemUnit")]
	public static void CreateUnit ()
	{
		SystemUnit su = ScriptableObject.CreateInstance<SystemUnit>();
		AssetDatabase.CreateAsset(su, "Assets/Config/Units.asset");
		AssetDatabase.SaveAssets();
	}
	
	[MenuItem("Assets/Create/SystemStats")]
	public static void CreateStats ()
	{
		SystemStats st = ScriptableObject.CreateInstance<SystemStats>();
		AssetDatabase.CreateAsset(st, "Assets/Config/Stats.asset");
		AssetDatabase.SaveAssets();
	}
}
