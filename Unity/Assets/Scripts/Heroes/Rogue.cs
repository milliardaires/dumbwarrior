﻿using UnityEngine;
using System.Collections;
using System;

public class Rogue : IPlayer 
{
	private const int MAX = 1000;

	public GameObject SpriteVision;
	public GameObject SpriteCrit;
	public Color AlphaFufu;

	private int CoolDownFufu =  3;
	private int m_coolDown = 0;

	void OnEnable()
	{
		Subscribe();
	}
	
	void OnDisable()
	{
		UnSubscribe();
	}

	#region Animation
	public enum AnimationRogue
	{
		IDLE = 1,
		VISION,
		FIGHT,
		HIT
		
	}
	private string idleAnim;
	//private string visionAnim;
	private string fightAnim;
	private string hitAnim;
	#endregion

	#region FSM
	public enum StateRogue
	{
		RandomM,
		AlerteM,
		VisionM,
		FightM,
		DieM
	}
	private FSM fsm;
	private State RandomState;
	//private State AlerteState;
	private State VisionState;
	private State FightState;
	private State DieState;
	#endregion

	public AnimationCurve AttackCurve;

	public override void Init(PlayersManager player, Unit unit, int[] bonus, int levelUnit)
	{
		m_playerMgr = player;
		m_poolDiamon = m_playerMgr.Pooler;
		RightSide = m_playerMgr.RightSide;
		
		SpriteRender.material = m_playerMgr.PlayerColor;
		LifeBar.renderer.material.color = m_playerMgr.PlayerColor.GetColor("_MaskColor");
		
		HP = unit.GetStatValueLevel(DataBase.TypeStats.HP, levelUnit) + bonus[(int)DataBase.TypeStats.HP];
		Vision = unit.GetStatValueLevel(DataBase.TypeStats.VISION, levelUnit) + bonus[(int)DataBase.TypeStats.VISION];
		Range = unit.GetStatValueLevel(DataBase.TypeStats.RANGE, levelUnit) + bonus[(int)DataBase.TypeStats.RANGE];
		if (Range <= 1)
			Damage = unit.GetStatValueLevel(DataBase.TypeStats.DAMAGE_CAC, levelUnit) + bonus[(int)DataBase.TypeStats.DAMAGE_CAC];
		else
			Damage = unit.GetStatValueLevel(DataBase.TypeStats.DAMAGE_DIST, levelUnit) + bonus[(int)DataBase.TypeStats.DAMAGE_DIST];
		Critique = unit.GetStatValueLevel(DataBase.TypeStats.CRIT, levelUnit) + bonus[(int)DataBase.TypeStats.CRIT];
		CoolDownFufu = unit.GetStatValue(DataBase.TypeStats.COOLDOWN_FUFU) + bonus[(int)DataBase.TypeStats.COOLDOWN_FUFU];
		
		m_currentLevel = levelUnit + 1;
		m_currentHP = HP;
		
		InitAnimation();
		
		InitFSM();

		//Invisible(true);
	}

	public override void InitFSM()
	{
		RandomState = new State((int)StateRogue.RandomM, RM_Enter, RM_Logic, RM_Movement, RM_Exit);
		//AlerteState = new State((int)StatePlayer.AlerteM, AM_Enter, null, AM_Movement, AM_Exit);
		VisionState = new State((int)StateRogue.VisionM, VM_Enter, VM_Logic, VM_Movement, VM_Exit);
		FightState = new State((int)StateRogue.FightM, FM_Enter, FM_Logic, null, FM_Exit);
		DieState = new State((int)StateRogue.DieM, DM_Enter, null, null, DM_Exit);

		fsm = new FSM();
		fsm.SetState(RandomState);
	}

	public override void InitAnimation()
	{
		string nameS = MySprite.name;
		string[] s = nameS.Split(DELIMIT);
		nameS = s[0] + DELIMIT + s[1];
		
		idleAnim = nameS + DELIMIT + m_currentLevel + DELIMIT + s[3] + DELIMIT + (int)AnimationRogue.IDLE;
		//visionAnim = nameS + DELIMIT + m_currentLevel + DELIMIT + s[3] + DELIMIT + (int)AnimationFarmer.VISION;
		fightAnim = nameS + DELIMIT + m_currentLevel + DELIMIT + s[3] + DELIMIT + (int)AnimationRogue.FIGHT;
		hitAnim = nameS + DELIMIT + m_currentLevel + DELIMIT + s[3] + DELIMIT + (int)AnimationRogue.HIT;
	}

	public override void OnLogicTick()
	{
		if (fsm != null)
			fsm.UpdateLogic();
	}
	
	public override void OnMovementTick()
	{
		if (fsm != null)
			fsm.UpdateMovement();
	}

	public override void GoDie()
	{
		fsm.ChangeState(DieState);
	}

	#region RANDOM MOVEMENT
	void RM_Enter()
	{
		MySprite = AnimatorManager.Instance[idleAnim];
	}
	void RM_Logic()
	{
		ComputeInvisibility();
		ComputeVisionAndAlerte();
	}
	void RM_Movement()
	{
		ComputeRandomMovement();
	}
	void RM_Exit()
	{}
	#endregion

	#region DIE
	void DM_Enter()
	{
		Tile.Taken = false;
		SpriteRender.sortingOrder = 3;
		m_playerMgr.UnitDie(this);
		IsDead = true;
		MySprite = AnimatorManager.Instance.RandomBlood();
		this.enabled = false;
	}
	void DM_Exit()
	{}
	#endregion

	#region FUFU
	void Invisible(bool enable)
	{
		IsVisible = !enable;
		if (IsVisible)
		{
			SpriteRender.color = Color.white;
			Color c = m_playerMgr.PlayerColor.GetColor("_MaskColor");
			SpriteRender.material.SetColor("_MaskColor", c);
			m_coolDown = 0;
		}
		else
		{
			SpriteRender.color = AlphaFufu;
			Color c = m_playerMgr.PlayerColor.GetColor("_MaskColor");
			c.a = AlphaFufu.a;
			SpriteRender.material.SetColor("_MaskColor", c);
			m_coolDown = CoolDownFufu;
		}
	}

	void ComputeInvisibility()
	{
		if (IsVisible)
		{
			++m_coolDown;
			if (m_coolDown > CoolDownFufu)
				Invisible(true);
		}
	}

	void ResetCoolDownFufu()
	{
		m_coolDown = 0;
	}
	#endregion

	#region VISION
	void VM_Enter()
	{
		GameObject obj = Utils.InstantiateChild(SpriteVision, transform);
		Destroy(obj, GameTick.Instance.TimeByTick);

		//MySprite = AnimatorManager.Instance[visionAnim];
		m_initiative = RandomManager.Range(0, 100);
		//Debug.Log(gameObject.name + " init " + m_initiative);
		VM_Movement();
	}
	void VM_Logic()
	{
		ComputeInvisibility();
		ComputeVision();

		if (m_targetEnemy == null)
		{
			fsm.ChangeState(RandomState);
		}
		else
		{
			// TODO : distance = 1 ( contact )
			if (ComputeRange(m_targetEnemy.Tile))
			{
				// TODO : goto state fight
				m_targetEnemy.IsAttackBy(this);
				fsm.ChangeState(FightState);				
			}
		}
	}
	void VM_Movement()
	{
		if (!ComputeRange(m_targetEnemy.Tile))
			Tracking();
	}

	void VM_Exit()
	{}
	#endregion

	#region FIGHT
	void FM_Enter()
	{
		ResetCoolDownFufu();
		ReplaceSprite(m_targetEnemy.Tile);

		InFight = true;
		if (m_initiative < m_targetEnemy.Initiative)
			m_attackOnTick = true;

	}
	void FM_Logic()
	{

		if (m_targetEnemy.IsDead)
		{
			ComputeVision();
			if (m_targetEnemy == null)
			{
				fsm.ChangeState(RandomState);
				return;
			}
		}

		if (!ComputeRange(m_targetEnemy.Tile))
		{
			fsm.ChangeState(VisionState);
			return;
		}

		if (m_attackOnTick)
		{
			if (Range <= 1)
				StartCoroutine(AttackTo(m_targetEnemy.Position));

			MySprite = AnimatorManager.Instance[hitAnim];
			//Debug.Log(gameObject.name + " ATK " + Damage[m_currentLevel]);
			// CRITIQUE
			int crit = 1;
			if (RandomManager.GetLucky(Critique))
			{
				GameObject obj = Utils.InstantiateChild(SpriteCrit, transform);
				Destroy(obj, GameTick.Instance.TimeByTick);
				crit = 2;
			}
			bool dead = m_targetEnemy.ReceiveDamage(Damage * crit);
			if (dead)
				m_playerMgr.KillUnit();
		}
		else
		{
			MySprite = AnimatorManager.Instance[fightAnim];
			Invisible(false);
		}
		m_attackOnTick = !m_attackOnTick;
	}
	void FM_Exit()
	{
		InFight = false;
		m_attackOnTick = false;
		m_initiative = 100;
	}

	IEnumerator AttackTo(Vector3 target)
	{		
		Vector3 diff = target - Position;
		
		Vector3 from = Position;
		float t = 0f;

		while (t < 1f)
		{
			t += TimeManager.DeltaTime / (GameTick.Instance.TimeByTick * GameTick.Instance.TimeByTick);			
			Position = from + (diff * AttackCurve.Evaluate(t));
			
			yield return new WaitForEndOfFrame();
		}
	}
	#endregion

	protected void Tracking()
	{
		// TODO : find the shortest way
		Pos2D target = m_targetEnemy.Tile.PositionGrid;

		// path finding
		int[] heuristic = {MAX, MAX, MAX, MAX};
		int min = MAX;
		ITile neighbour;

		// test neighbours
		for( int i = 0; i < MaxDirection; i++)
		{
			neighbour = Tile.GetNeighbours(i);
			if (neighbour != null)
			{
				if (neighbour.CanGo /*&& !HasHole(neighbour) */&& neighbour != LastTile)
				{
					heuristic[i] = target.Distance(neighbour.PositionGrid); // manhattan
					if (min > heuristic[i])
						min = heuristic[i];
				}
			}
		}
		// TODO : get the shortest
		for( int i = 0; i < MaxDirection; i++)
		{
			if (heuristic[i] == min)
			{
				GoNeighbour(i);
				break;
			}
		}
	}

	protected void ComputeVision()
	{
		int minDist = MAX;
		// first alerte -> vision
		foreach(IPlayer enemy in m_playerMgr.Opponent.CurrentPlayers)
		{
			int distance = Tile.PositionGrid.Distance(enemy.Tile.PositionGrid);
			if ( distance < Vision)
			{
				if (distance < minDist)
				{
					minDist = distance;
					m_targetEnemy = enemy;
				}
			}
		}
		if (minDist == MAX)
			m_targetEnemy = null;
	}

	protected void ComputeVisionAndAlerte()
	{
		// first alerte -> vision
		foreach(IPlayer enemy in m_playerMgr.Opponent.CurrentPlayers)
		{
			int distance = Tile.PositionGrid.Distance(enemy.Tile.PositionGrid);
			if ( distance < Vision)
			{
				if (enemy.InFight)
				{
					// goto state alerte, tracking tile of fight
					m_targetEnemy = enemy;
					// todo : mode ALERTE
					fsm.ChangeState(VisionState);
					return;
				}
				else
				{				
					// TODO : player isn't in fight, test vision
					if (distance < Vision)
					{
						// TODO : distance = 2 ( next tick -> fight)
						if (distance == Range + 1)
						{
							// TODO : this move and player first attack
							m_targetEnemy = enemy;
							fsm.ChangeState(FightState);
						}
						
						// TODO : distance = 1 ( contact )
						else if (distance == Range)
						{
							// TODO : goto state fight
							m_targetEnemy = enemy;
							fsm.ChangeState(FightState);
							// TODO : random to decide who begins

						}
						else
						{
							// TODO : goto state tracking player position 2D
							m_targetEnemy = enemy;
							fsm.ChangeState(VisionState);
						}
						return;
					}
				}
			}
		}
	}
}
