﻿using UnityEngine;
using System.Collections;
using System;

public class Farmer : IPlayer 
{
	private const int MAX = 1000;
	private Diamon m_targetDiamon;
	//private event Action OnTile;
	//private event Action OnCloseTile;

	void OnEnable()
	{
		Subscribe();
	}
	
	void OnDisable()
	{
		UnSubscribe();
	}

	#region Animation
	public enum AnimationFarmer
	{
		IDLE = 1,
		VISION = 2,
		CATCH = 4,
		DIAMON = 5

	}
	private string idleAnim;
	private string visionAnim;
	//private string catchAnim;
	private string diamonAnim;
	#endregion

	#region FSM
	public enum StateFarmer
	{
		RandomM,
		VisionM,
		ReturnM,
		FightM,
		DieM
	}
	private FSM fsm;
	private State RandomState;
	private State VisionState;
	private State ReturnState;
	private State FightState;
	private State DieState;
	#endregion
		
	public override void InitFSM()
	{
		RandomState = new State((int)StateFarmer.RandomM, RM_Enter, RM_Logic, RM_Movement, RM_Exit);
		FightState = new State((int)StateFarmer.FightM, FM_Enter, null, FM_Movement, FM_Exit);
		VisionState = new State((int)StateFarmer.VisionM, VM_Enter, VM_Logic, VM_Movement, VM_Exit);
		ReturnState = new State((int)StateFarmer.ReturnM, RDM_Enter, RDM_Logic, RDM_Movement, RDM_Exit);
		DieState = new State((int)StateFarmer.DieM, DM_Enter, null, null, DM_Exit);
		
		fsm = new FSM();
		fsm.SetState(RandomState);
	}

	public override void InitAnimation()
	{
		string nameS = MySprite.name;
		string[] s = nameS.Split(DELIMIT);
		nameS = s[0] + DELIMIT + s[1];

		idleAnim = nameS + DELIMIT + m_currentLevel + DELIMIT + s[3] + DELIMIT + (int)AnimationFarmer.IDLE;
		visionAnim = nameS + DELIMIT + m_currentLevel + DELIMIT + s[3] + DELIMIT + (int)AnimationFarmer.VISION;
		//catchAnim = nameS + DELIMIT + m_currentLevel + DELIMIT + s[3] + DELIMIT + (int)AnimationFarmer.CATCH;
		diamonAnim = nameS + DELIMIT + m_currentLevel + DELIMIT + s[3] + DELIMIT + (int)AnimationFarmer.DIAMON;
	}

	public override void OnLogicTick()
	{
		if (fsm != null)
			fsm.UpdateLogic();
	}

	public override void OnMovementTick()
	{
		if (fsm != null)
			fsm.UpdateMovement();
	}
	
	public override void GoDie()
	{
		fsm.ChangeState(DieState);
	}
	
	#region RANDOM MOVEMENT
	void RM_Enter()
	{
		MySprite = AnimatorManager.Instance[idleAnim];
		m_targetDiamon = null;
	}
	void RM_Logic()
	{
		ComputeVision();
	}
	void RM_Movement()
	{
		ComputeRandomMovement();
	}
	void RM_Exit()
	{}
	#endregion

	#region Fight
	void FM_Enter()
	{
		ReplaceSprite(m_targetEnemy.Tile);
		InFight = true;
	}	

	void FM_Movement()
	{
		if (!TakeDamage)
			fsm.ReturnState();

		TakeDamage = false;
	}
	void FM_Exit()
	{
		InFight = false;
	}
	public override void IsAttackBy(IPlayer player)
	{
		m_targetEnemy = player;
		fsm.ChangeState(FightState);
	}
	#endregion

	#region DIE
	void DM_Enter()
	{
		Tile.Taken = false;
		//SpriteRender.sortingOrder = 2;
		m_playerMgr.UnitDie(this);
		IsDead = true;
		MySprite = AnimatorManager.Instance.RandomBlood();
		this.enabled = false;
	}
	void DM_Exit()
	{}
	#endregion
	
	#region VISION
	void VM_Enter()
	{
		MySprite = AnimatorManager.Instance[visionAnim];
		//OnTile += ReturnDiamon;
	}
	void VM_Logic()
	{
		if (m_targetDiamon == null)
			fsm.ChangeState(RandomState);
//		else
//		{
//			//Pos2D target = m_targetDiamon.PositionGrid;
//			int distance = Tile.PositionGrid.Distance(m_targetDiamon.PositionGrid);
//			//Debug.Log(distance);
//			// TODO : trigger
//			if (distance == 0)
//			{
//				ReturnDiamon();
//			}
//		}
	}

	void VM_Movement()
	{
		Tracking(m_targetDiamon.PositionGrid);
	}

	void VM_Exit()
	{
		//OnTile -= ReturnDiamon;
	}
	#endregion
	
	#region Return diamon
	void RDM_Enter()
	{
		MySprite = AnimatorManager.Instance[diamonAnim];
		m_targetDiamon.Catched(); // pop a new diamon
		//OnCloseTile += ReturnRandom;
	}
	void RDM_Logic()
	{
		//Pos2D target = m_targetDiamon.PositionGrid;
		int distance = Tile.PositionGrid.Distance(Treasure.PositionGrid);
		//Debug.Log(distance);
		// TODO : trigger
		if (distance == 1)
		{
			ReturnRandom();
		}
	}

	void RDM_Movement()
	{
		Tracking(Treasure.PositionGrid);
	}
	void RDM_Exit()
	{
		AddDiamon();
		//OnCloseTile -= ReturnRandom;
	}
	#endregion

	void ReturnDiamon()
	{
		fsm.ChangeState(ReturnState);
	}

	void ReturnRandom()
	{
		fsm.ChangeState(RandomState);
	}

	protected void Tracking(Pos2D target)
	{
		// TODO : find the shortest way

		// path finding
		int[] heuristic = {MAX, MAX, MAX, MAX};
		int min = MAX;
		ITile neighbour;
		
		// test neighbours
		for( int i = 0; i < MaxDirection; i++)
		{
			neighbour = Tile.GetNeighbours(i);
			if (neighbour != null)
			{
				if (neighbour.CanGo && !HasHole(neighbour) && neighbour != LastTile)
				{
					heuristic[i] = target.Distance(neighbour.PositionGrid); // manhattan
					if (min > heuristic[i])
						min = heuristic[i];
				}
			}
		}

		if (min == 0)
		{
			ReturnDiamon();
		}
		// TODO : get the shortest
		for( int i = 0; i < MaxDirection; i++)
		{
			if (heuristic[i] == min)
			{
				GoNeighbour(i);
				break;
			}
		}
	}
	
	protected void ComputeVision()
	{
		// first alerte -> vision
		int minDist = MAX;
		foreach(Diamon diamon in m_poolDiamon.Diamons)
		{
			// TODO : get the shortest
			int distance = Tile.PositionGrid.Distance(diamon.PositionGrid);
			if ( distance < Vision)
			{						
				if (distance < minDist)
				{
					minDist = distance;
					// TODO : goto state tracking diamon position 2D
					m_targetDiamon = diamon;
				}
			}
		}

		if (m_targetDiamon != null)
		{
			fsm.ChangeState(VisionState);
		}
	}
}
