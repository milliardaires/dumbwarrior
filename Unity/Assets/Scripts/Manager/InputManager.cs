﻿using UnityEngine;
using System.Collections;

public class InputManager : Singleton<InputManager> 
{
	public enum Players
	{
		P1 = 0,
		P2 = 1,
		P3 = 2,
		P4 = 3,
	}

	public InputConfig Config;

	//private PlayerInput[] Inputs;
	private int m_countPlayer = 0;
	public int CountPlayer
	{
		get {return m_countPlayer;}
		set {m_countPlayer = value;}
	}

	public PlayerInput this[int id]
	{
		get 
		{
			if (id < 0 || id > 3)
			{
				//Debug.LogError("index input fail");
				return Config.Inputs[0];;
			}
			return Config.Inputs[id];
		}
	}

	// Use this for initialization
	void Start () 
	{
		if (Config == null)
			Debug.LogError("no config input");
		m_isReady = true;
	}
}
