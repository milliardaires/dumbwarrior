﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimatorManager : Singleton<AnimatorManager> 
{
	//public Sprite test;
	public string atlas;

	private const string dieAnim = "S_Players_Bonus_";
	private const int DIE_BEGIN = 4;
	private const int DIE_END = 8;

	#region Dictionary
	private Dictionary<string, Sprite> m_dicoSprite;
	public Sprite this[string n]
	{
		get 
		{
			//Debug.Log("request : " + n);
			try
			{
				return m_dicoSprite[n];
			}
			catch(System.Exception ex)
			{
				Debug.Log("fail request : " + n + " " + ex.ToString());
				return null;
			}
		}
	}
	#endregion

	void Start()
	{
		m_dicoSprite = new Dictionary<string, Sprite>();

		BuildDico();

		m_isReady = true;
	}

	void BuildDico()
	{
		// get all sprites
		Sprite[] sprites = Resources.LoadAll<Sprite>(atlas);

		// fill the dico
		foreach(Sprite s in sprites)
		{
			m_dicoSprite.Add(s.name, s);
		}
	}

	#region Blood
	public Sprite RandomBlood()
	{
		return this[dieAnim + RandomManager.Range(DIE_BEGIN, DIE_END)];
	}
	#endregion
}
