﻿using UnityEngine;
using System.Collections;

public class InfoManager : Singleton<InfoManager> 
{	
	public const int MAX_PLAYERS = 4;

	public int CurrentStage = 0;
	public Sprite[] ColorByPlayer;
	public Sprite[] DrapByPlayer;
	public Sprite[] LifeByPlayer;
	public Material[] MaterialByPlayer;

	private int[] m_ids;
	private int m_idx;

	public SystemStats StatsAssets;
	public SystemUnit UnitAssets;

	// Use this for initialization
	void Start () 
	{
		// NEW GAME
		InitPlayers(MAX_PLAYERS);
#if UNITY_EDITOR
		PlayerInfo p1 = m_players[0];
		//PlayerInfo p2 = m_players[1];
		p1.Xp = 23;
		p1.Win = true;
		p1.Gold = 100;
//		p1.Death = 10;
//		p1.Kills = 100;
//		p1.DiamsUsed = 5;
		p1.XpWin = 2500;
		p1.AddNewItem(ItemsManager.Instance.GetItem("sword"));
//		p1.AddNewItem(ItemsManager.Instance.GetItem("Coffre2"));
//		p1.AddNewItem(ItemsManager.Instance.GetItem("Coffre3"));
#endif
		m_isReady = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		/*if (Input.GetKeyDown(KeyCode.Space))
		{
			PlayerInfo p1 = m_players[0];
			p1.Gold = 100;
			p1.Xp = 23;
			p1.Stage = 3;
			p1.Win = true;
			p1.AddNewItem(new Item());
			p1.AddUnit(new Unit("Mage", 3));
			p1.Stats.Add(new PlayerStat(3));
			FileManager.SaveGame("Game1", p1);
		}

		if (Input.GetKeyDown(KeyCode.M))
			m_players[0] = FileManager.LoadGame("Game1");*/
	}

	#region Stats
	public int GetStatValue(int level, DataBase.TypeStats stat)
	{
		return StatsAssets.GetStatValue(level, stat);
	}
	
	public PlayerStat GetStatInfo(DataBase.TypeStats stat)
	{
		return StatsAssets.GetStatInfo(stat);
	}
	#endregion

	#region Player
	private PlayerInfo[] m_players;
	public PlayerInfo[] Players
	{
		get { return m_players; }
	}

	public int CountPlayer
	{
		get { return m_players.Length;}
	}
	void InitPlayers(int count)
	{
		m_ids = new int[2];
		m_idx = 0;

		m_players = new PlayerInfo[count];
		for(int i = 0; i < count; ++i)
		{
			m_players[i] = new PlayerInfo(i);
			// TODO : set Unit Base
			foreach(Unit unit in UnitAssets.Units)
			{
				m_players[i].AddUnit(unit.Clone());
			}
			// TODO : set Stats Base
			foreach(PlayerStat ps in StatsAssets.Stats)
			{
				m_players[i].Stats.Add(ps.Clone());
			}
		}
	}
	
	public void ActivePlayers(int p)
	{
		//Debug.Log(p);
		m_ids[m_idx] = p;
		m_players[p].Active(CurrentStage + 1);
		++m_idx;
	}

	public void SelectPlayer(int i)
	{
		m_players[i].Selected = true;
	}

	public int CountPlayerPlaying()
	{
		int count = 0;
		for(int i = 0; i < m_players.Length; ++i)
			if (m_players[i].Playing)
				++count;
		return count;
	}

	// [0,1]
	public int GetIdPlayer(int id)
	{
		return m_ids[id];
	}

	public PlayerInfo GetPlayer(int id)
	{
		int count = 0;
		foreach(PlayerInfo p in m_players)
		{
			if (p.Playing)
			{
				if (count == id)
					return p;
				++count;
			}
		}
		return m_players[0];
	}

	public void NextStage()
	{
		++CurrentStage;
	}

	public void ResetPlayers()
	{
		m_idx = 0;
		m_ids[0] = 0;
		m_ids[1] = 0;

		foreach(PlayerInfo player in m_players)
		{
			player.Reset();
		}
	}
	#endregion

	#region COLOR
	public Sprite GetColorPlayer(int id)
	{
		return ColorByPlayer[m_ids[id]];
	}
	
	public Sprite GetDrapPlayer(int id)
	{
		return DrapByPlayer[m_ids[id]];
	}
	
	public Sprite GetLifeColorPlayer(int id)
	{
		return LifeByPlayer[m_ids[id]];
	}

	public Material GetMaterialPlayer(int id)
	{
		return MaterialByPlayer[m_ids[id]];
	}
	#endregion
}
