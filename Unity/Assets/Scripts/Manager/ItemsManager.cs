﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ItemsManager : Singleton<ItemsManager> 
{
	public Inventaire ItemsAssets;

	private List<Item> m_containers = new List<Item>();

	public void Start()
	{
		m_containers = ItemsAssets.Items.Where(i => i.Container).OrderBy(i => i.Rate).ToList();

		m_isReady = true;
	}

	public Item GetItem(string nameItem)
	{
		foreach(Item item in ItemsAssets.Items)
		{
			if (nameItem.Equals(item.Name))
			{
				return item;
			}
		}
		return null;
	}

	public Item GetItem(DataBase.Category cat)
	{
		List<Item> random = ItemsAssets.Items.Where(i => i.category == cat && !i.Container).ToList();
		return random[RandomManager.Range(0, random.Count - 1)];
	}

	public Item GetContainer()
	{
		// random
		int rand = RandomManager.Range(0, 99);
		int rate = 0;
		foreach(Item item in m_containers)
		{
			if (rate <= rand && rand < (rate + item.Rate))
			{
				return item;
			}
			rate += item.Rate;
		}
		return m_containers[m_containers.Count - 1];
	}
}
