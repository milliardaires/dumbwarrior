﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class PlayersManager : MonoBehaviour, ITicked
{
	public bool RightSide = false;
	[HideInInspector]
	public Material PlayerColor;
	
	public Spawn Spawner;
	public int BeginDiamon = 3;

	public PlayersManager Opponent;
	private List<IPlayer> m_players = new List<IPlayer>();
	public List<IPlayer> CurrentPlayers
	{
		get { return m_players;}
	}

	public PoolDiamon Pooler;
	public IPlayer KingPrefab;
	private IPlayer m_king;

	private const int MAX_DIAMON = 4;

	private int m_countDiamon;
	public int CountDiam
	{
		get { return m_countDiamon; }
		set 
		{ 
			m_countDiamon = Mathf.Clamp(value, 0, MAX_DIAMON); 
			for(int i = 0; i < UiDiams.Length; ++i)
				UiDiams[i].gameObject.SetActive((i < m_countDiamon));
		}
	}
	private int m_countTick = 0;

	public int XpToLevel = 10;
	private int m_xp = 0;
	private bool m_levelUp = false;
	public int XP
	{
		get { return m_xp; }
		set
		{
			m_xp = value;
			if ( m_xp >= XpToLevel)
			{
				m_xp = 0;
				++Level;
				m_levelUp = true;
				foreach(UiGameDiam diams in UiDiams)
					diams.LevelUp();
			}
			Vector3 scaleXp = UiXp.localScale;
			scaleXp.x = Mathf.Clamp01( (float)m_xp / (float)XpToLevel);
			UiXp.localScale = scaleXp; 
		}
	}

	public int Level = 0;
	public int DiamonByTick = 10;

	// Recap
	private DataBase.StateGame State;
	private int m_countKill = 0;
	private int m_countDeath = 0;
	private int m_countDiamsUsed = 0;

	// UI
	public GameObject UiDiam;
	private UiGameDiam[] UiDiams;
	public Transform UiHp;
	public Transform UiXp;

	#region Queue Action
	private Queue<Action> m_actions = new Queue<Action>();
	#endregion

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!MasterManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		InitPlayer();

		//GET Ui
		//int id = (RightSide ? 2 : 1);
		UiDiams = UiDiam.GetComponentsInChildren<UiGameDiam>();
		UiDiams = UiDiams.OrderBy(x => x.name).ToArray();
		foreach(UiGameDiam diams in UiDiams)
			diams.Init();

		XP = 0;

		while(!TileMapper.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		Spawner.PositionGrid = Spawner.PositionGrid;

		CountDiam = BeginDiamon;

		while(!AnimatorManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		State = DataBase.StateGame.PLAYING;

		// FIRST KING
		Unit unit = m_playerInfo.GetKing();
		m_actions.Enqueue(() => {AddKing(unit);} );
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_playerInfo != null)
			UpdateInput();

		if (m_king && State == DataBase.StateGame.PLAYING)
		{
			if (!m_king.IsDead)
			{
				Vector3 life = m_king.LifeBar.localScale;
				UiHp.localScale = life;
			}
			else
			{
				Opponent.Win();
				this.Loose();
				StartCoroutine("GoToLoot");
			}
		}
	}

	public void Win()
	{
		State = DataBase.StateGame.WIN;
		m_playerInfo.EndGame(true, m_countKill, m_countDeath, m_countDiamsUsed, m_xp);
	}

	public void Loose()
	{
		State = DataBase.StateGame.LOOSE;
		m_playerInfo.EndGame(false, m_countKill, m_countDeath, m_countDiamsUsed, m_xp);
	}

	IEnumerator GoToLoot()
	{
		yield return new WaitForSeconds(2f);
		Application.LoadLevel("Loot");
		//GameTick.Instance.End();
	}

	void UpdateInput()
	{
		if (InputManager.Instance[m_playerInfo.Id].PressA())
		{
			GrabUnit(4);
		}
		else if (InputManager.Instance[m_playerInfo.Id].PressB())
		{
			GrabUnit(5);
		}
		else if (InputManager.Instance[m_playerInfo.Id].PressDown())
		{
			GrabUnit(2);
		}
		else if (InputManager.Instance[m_playerInfo.Id].PressRight())
		{
			GrabUnit(3);
		}
		else if (InputManager.Instance[m_playerInfo.Id].PressUp())
		{
			GrabUnit(1);
		}
		else if (InputManager.Instance[m_playerInfo.Id].PressLeft())
		{
			GrabUnit(0);
		}
	}

	public void OnLogicTick()
	{
		if (m_actions.Count > 0)
		{
			Action action = m_actions.Dequeue();
			if (action != null)
				action();
		}

		++m_countTick;
		if (m_countTick >= DiamonByTick)
		{
			m_countTick = 0;
			++CountDiam;
		}
	}

	public void OnMovementTick()
	{}

	void AddDiamon()
	{
		CountDiam = Mathf.Clamp(CountDiam + 1, 0, MAX_DIAMON);
	}

	public void KillUnit()
	{
		XP = XP + (1 + m_bonus[(int)DataBase.TypeStats.XP]);
		++m_countKill;
	}

	public void UnitDie(IPlayer unit)
	{
		m_players.Remove(unit);
		++m_countDeath;
	}

	void GrabUnit(int i)
	{
		// TODO : get unit and get prefab
		Unit unit = m_playerInfo.GetCurrentUnit(i);
		if (unit != null)
		{
			IPlayer prefab = DataUnit.Instance.GetUnit(unit.Name);
			int cost = unit.GetStatValue(DataBase.TypeStats.COST);
			if (cost <= m_countDiamon)
			{
				CountDiam = CountDiam - cost;
				m_countDiamsUsed += cost;
				m_actions.Enqueue( () => { CreateUnit(prefab, unit); });
			}
		}
	}

	void CreateUnit(IPlayer prefab, Unit unit)
	{
		IPlayer player = Spawner.SpawnPlayer(prefab);
		// TYPE FARMER
		if (player.GetComponent<Farmer>() != null)
		{
			player.OnCatchDiamon += AddDiamon;
			player.Treasure = (ITile)Spawner;
		}
		AddPlayer(player, unit);
	}

	void AddKing(Unit unit)
	{
		m_king = Spawner.SpawnPlayer(KingPrefab);
		AddPlayer(m_king, unit);
	}

	void AddPlayer(IPlayer player, Unit unit)
	{
		// TODO : init whit UNIT
		player.Init(this, unit, m_bonus, m_levelUp ? 1 : 0);
		m_players.Add(player);

		if(m_levelUp)
		{
			m_levelUp = false;
			foreach(UiGameDiam diams in UiDiams)
				diams.ResetLevel();
		}
	}

	#region Bonus
	private PlayerInfo m_playerInfo;
	private int[] m_bonus;
	void InitPlayer()
	{
		m_bonus = new int[(int)DataBase.TypeStats.MAX];
		int id = RightSide ? 1 : 0;
		m_playerInfo = InfoManager.Instance.GetPlayer(id);
		PlayerColor = InfoManager.Instance.GetMaterialPlayer(id);

		foreach(PlayerStat s in m_playerInfo.Stats)
		{
			m_bonus[(int)s.stat.type] = s.GetStatValue();
		}

		foreach(Item item in m_playerInfo.CurrentItems)
		{
			foreach(Stats s in item.stats)
			{
				m_bonus[(int)s.type] += s.First;
			}
		}

		DiamonByTick += m_bonus[(int)DataBase.TypeStats.DELAY];
		BeginDiamon += m_bonus[(int)DataBase.TypeStats.DIAMS];
		// TODO : affect all units (hp, cac, etc)
		/*m_bonus[(int)Item.TypeStats.CAC]
		m_bonus[(int)Item.TypeStats.CRIT]
		m_bonus[(int)Item.TypeStats.DIST_ATK]
		m_bonus[(int)Item.TypeStats.HP]
		m_bonus[(int)Item.TypeStats.RANG]*/
	}
	#endregion

	void OnEnable()
	{
		Subscribe();
	}
	
	void OnDisable()
	{
		UnSubscribe();
	}

	public void Subscribe()
	{
		GameTick.Instance.UpdateLogic += OnLogicTick;
	}
	
	public void UnSubscribe()
	{
		if (GameTick.Instance != null)
			GameTick.Instance.UpdateLogic -= OnLogicTick;
	}
}
