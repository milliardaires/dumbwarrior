﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataUnit : Singleton<DataUnit>
{
	public IPlayer[] Units;

	private Dictionary<string, IPlayer> m_dicoUnit = new Dictionary<string, IPlayer>();

	// Use this for initialization
	void Start () 
	{
		FillDico();
	}

	void FillDico()
	{
		foreach(IPlayer unit in Units)
		{
			m_dicoUnit.Add(unit.name, unit); //  Sprite => Unit
		}
	}

	public IPlayer GetUnit(string name)
	{
		return m_dicoUnit[name];
	}
}
