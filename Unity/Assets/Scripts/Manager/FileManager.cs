﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

public static class FileManager 
{
	public const string PATH = "/Saves/";
	public const string EXT = ".xml";


	public static void SaveGame(string nameGame, PlayerInfo info)
	{
		//BitStream stream = new BitStream();
		/*FileStream stream = File.Create(Application.dataPath + PATH + nameGame + EXT);
		BinaryFormater formatter = new BinaryFormatter();
		formatter.Serialize(stream, info);
		stream.Close();*/

		// Insert code to set properties and fields of the object.
		XmlSerializer mySerializer = new XmlSerializer(typeof(PlayerInfo));
		// To write to a file, create a StreamWriter object.
		StreamWriter myWriter = new StreamWriter(Application.dataPath + PATH + nameGame + EXT);
		mySerializer.Serialize(myWriter, info);
		myWriter.Close();
	}

	public static PlayerInfo LoadGame(string nameGame)
	{
		/*FileStream stream = File.OpenRead(Application.dataPath + PATH + nameGame + EXT);
		BinaryFormater formatter = new BinaryFormatter();
		PlayerInfo info = (PlayerInfo) formatter.Deserialize(stream);
		stream.Close();*/

		XmlSerializer mySerializer = new XmlSerializer(typeof(PlayerInfo));
		// To read the file, create a FileStream.
		FileStream myFileStream = new FileStream(Application.dataPath + PATH + nameGame + EXT, FileMode.Open);
		// Call the Deserialize method and cast to the object type.
		PlayerInfo info = (PlayerInfo) mySerializer.Deserialize(myFileStream);
		return info;
	}
}
