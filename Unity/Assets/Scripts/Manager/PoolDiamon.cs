﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolDiamon : MonoBehaviour 
{
	public ITile DiamonPrefab;
	public int MaxCount = 4;

	private List<Diamon> m_diamons = new List<Diamon>();
	public List<Diamon> Diamons
	{
		get { return m_diamons; }
	}

	public Pos2D randomRange;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!TileMapper.Instance.IsReady)
		{
			yield return new WaitForEndOfFrame();
		}


		for (int i = 0; i < MaxCount; ++i)
			RandomPop();
	}

	void NewDiamon(Diamon diam)
	{
		m_diamons.Remove(diam);
		if (diam != null)
			Destroy(diam.gameObject);
		RandomPop();
	}

	void RandomPop()
	{
		ITile obj = TileMapper.Instance.InstantiateRandomOnGrid(randomRange, DiamonPrefab, transform);

		Diamon diamon = obj.GetComponent<Diamon>();
		diamon.OnCatch += NewDiamon;

		m_diamons.Add(diamon);
	}
}
