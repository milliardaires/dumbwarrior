﻿using UnityEngine;
using System.Collections;

public class TimeManager 
{
	private const float timeScale = 1f;

	public static float DeltaTime
	{
		get { return Time.deltaTime;}
	}

	public static float CurrentTime
	{
		get { return Time.time;}
	}

	public static void Pause(bool pause)
	{
		Time.timeScale = pause ? 0 : timeScale;
	}

}
