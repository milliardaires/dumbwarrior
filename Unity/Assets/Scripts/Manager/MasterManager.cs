﻿using UnityEngine;
using System.Collections;

public class MasterManager : Singleton<MasterManager> 
{
	//private InputManager m_inputMgr;
	//private InfoManager m_infoMgr;
	//private ItemsManager m_itemMgr;
	//private StatsManager m_statsMgr;
	//private UnitManager m_unitsMgr;

	// Use this for initialization
	IEnumerator Start () 
	{
		DontDestroyOnLoad(this);

		while(!InputManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		while(!InfoManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		while(!ItemsManager.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		m_isReady = true;
	}
	
	// Update is called once per frame
//	void Update () 
//	{
//	
//	}
}
