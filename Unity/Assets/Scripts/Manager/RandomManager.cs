﻿using UnityEngine;
using System.Collections;

public class RandomManager 
{
	public static float Range(float min, float max)
	{
		return Random.Range(min, max);
	}

	public static int Range(int min, int max)
	{
		return Random.Range(min, max + 1);
	}

	public static Vector2 InsideCircle()
	{
		return Random.insideUnitCircle;
	}

	public static Vector3 InsideSphere()
	{
		return Random.insideUnitSphere;
	}

	public static bool GetBool()
	{
		return Range (0,1) == 0;
	}

	public static bool GetLucky(int percent)
	{
		return Range (0,100) <= percent;
	}

	public static IMovable.Direction GetDirection()
	{
		return (IMovable.Direction)Range (1,4);
	}
}
