﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour 
{
	public Material MatRtt;

	private Camera m_camera;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!TileMapper.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		m_camera = gameObject.camera;
		RenderTexture rtt = new RenderTexture(2540, 1280, 1);
		rtt.Create();
		m_camera.targetTexture = rtt;
		MatRtt.mainTexture = rtt;

		ResizeCamera();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//m_camera.orthographicSize = Screen.height / 2f;
		//m_camera.aspect = 2f;
	}

	void ResizeCamera()
	{
		Grid grid = TileMapper.Instance.GRID;
		m_camera.aspect = 2f;
		m_camera.transform.position = new Vector3( grid.width * grid.sizeX / 2 , grid.height * grid.sizeY / 2, -10);
	}
}
