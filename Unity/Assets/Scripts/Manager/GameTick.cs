﻿using UnityEngine;
using System.Collections;

public class GameTick : Singleton<GameTick> 
{
	public float TimeByTick = 1f;
	private float m_countTime;

	public bool Ready = false;

	private int m_countTick;
	public int CountTick
	{
		get { return m_countTick;}
	}

	public delegate void UpdateTick();
	public event UpdateTick UpdateMovement;
	public event UpdateTick UpdateLogic;

	// Use this for initialization
	void Start () 
	{
		
	}

	public void Begin()
	{
		Ready = true;
		m_countTick = 0;
		m_countTime = 0f;
	}

	public void End()
	{
		Ready = false;
		m_countTick = 0;
		m_countTime = 0f;

//		UpdateMovement = null;
//		UpdateLogic = null;
	}

	// Update is called once per frame
	void Update () 
	{
		if (Ready)
		{
			m_countTime += TimeManager.DeltaTime;
			if (m_countTime > TimeByTick)
			{
				if (UpdateLogic != null)
					UpdateLogic();

				if (UpdateMovement != null)
					UpdateMovement();

				m_countTime = 0f;
				++m_countTick;
			}
		}
	}
}
