﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{
	public float Limit = 1f;

	public float SpeedZoom = 1f;
	public float SpeedDeplacement = 2f;
	public float OrthoSizeMax = 5f;
	public float OrthoSizeMin = 1f;

	public PlayersManager[] Players;

	private Transform m_trans;
	private Camera m_cam;
	private int m_width;
	private int m_height;
	private float m_minOrtho;

	private Vector2 Pos;
	private float OrthoSize;

	// Use this for initialization
	IEnumerator Start () 
	{
		while(!TileMapper.Instance.IsReady)
			yield return new WaitForEndOfFrame();

		m_trans = transform;
		m_cam = GetComponent<Camera>();
		m_minOrtho = OrthoSizeMin / OrthoSizeMax;
		m_width = TileMapper.Instance.GRID.width / 2;
		m_height = TileMapper.Instance.GRID.height / 2;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//InputDebug();

		ComputePosition();
		ComputeVision();

		UpdatePosition();
		UpdateVision();
	}

	void ComputePosition()
	{
		Vector2 avg = Vector2.zero;
		Vector2 posMax = Vector2.zero;
		Vector2 posMin = new Vector2(TileMapper.Instance.GRID.width, 0);
		int count = 0;

		foreach(PlayersManager player in Players)
		{
			foreach(IPlayer warrior in player.CurrentPlayers)
			{
				if (warrior.tag.Equals("Player"))
				{
					int x = warrior.Tile.PositionGrid.X;
					if (x > posMax.x)
					{
						posMax.x = x;
						posMax.y = warrior.Tile.PositionGrid.Y;
					}
					if (x < posMin.x)
					{
						posMin.x = x;
						posMin.y = warrior.Tile.PositionGrid.Y;
					}
					++count;
				}
			}
		}

		if (count > 0)
		{
			count = 2;
			avg = posMax + posMin;
			avg /= count;
			avg.x -= m_width;
			avg.x /= m_width; // [-1,1]
			avg.x *= Limit; // [-10,10]

			avg.y -= m_height;
			avg.y /= m_height; // [-1,1]
			avg.y *= Limit / 2f; // [-5,5]
		}

		Pos = avg;
		float dist = Mathf.Abs(posMax.x - posMin.x);
		//Debug.Log(dist);
		m_minOrtho = OrthoSizeMin / OrthoSizeMax;
		dist = Mathf.Clamp(dist /TileMapper.Instance.GRID.width, m_minOrtho, 1f);
		OrthoSize = dist * OrthoSizeMax;
	}

	void UpdatePosition()
	{
		Vector3 pos = m_trans.localPosition;
		pos.x = Mathf.Lerp(pos.x, Pos.x, TimeManager.DeltaTime * SpeedDeplacement);
		pos.y = Mathf.Lerp(pos.y, Pos.y, TimeManager.DeltaTime * SpeedDeplacement);
		m_trans.localPosition = pos;
	}

	void ComputeVision()
	{
	}

	void UpdateVision()
	{
		m_cam.orthographicSize = Mathf.Lerp(m_cam.orthographicSize, OrthoSize, TimeManager.DeltaTime * SpeedZoom);
	}

	void InputDebug()
	{
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			Vector3 pos = m_trans.localPosition;
			pos.x = Mathf.Lerp(pos.x, -Limit, TimeManager.DeltaTime * SpeedDeplacement);
			m_trans.localPosition = pos;
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			Vector3 pos = m_trans.localPosition;
			pos.x = Mathf.Lerp(pos.x, Limit, TimeManager.DeltaTime * SpeedDeplacement);
			m_trans.localPosition = pos;
		}
		else if (Input.GetKey(KeyCode.UpArrow))
		{
			m_cam.orthographicSize = Mathf.Lerp(m_cam.orthographicSize, 1f, TimeManager.DeltaTime * SpeedZoom);
		}
		else if (Input.GetKey(KeyCode.DownArrow))
		{
			m_cam.orthographicSize = Mathf.Lerp(m_cam.orthographicSize, 5f, TimeManager.DeltaTime * SpeedZoom);
		}
	}
}
