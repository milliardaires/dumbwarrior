﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileMapper : Singleton<TileMapper> 
{
	// HeighMap
	public Texture2D HeightMap;
	public int SizeTileX, SizeTileY;
	public Texture2D[] ListMaps;

	// Size in tile
	private int m_countTileX, m_countTileY;

	private Transform m_trans;
	private Transform[] Parents;
	private int[] counter;

	private Grid myGrid;
	public Grid GRID
	{
		get { return myGrid;}
	}

	public Environnement BiomEnv;
	private string BiomSelected;
	public int idTypeBiom;

	// Use this for initialization
	void Start () 
	{
		Init();
		BuildMap();
		m_isReady = true;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			NextBiom();
		}
	}

	void NextBiom()
	{
		idTypeBiom = (idTypeBiom + 1) % BiomEnv.TypeOfBiom.Count;
		BiomSelected = BiomEnv.TypeOfBiom[idTypeBiom];

		for (int y= 0; y < m_countTileY; ++y) // WIDTH
		{
			for (int x = 0; x < m_countTileX; ++x) // HEIGHT
			{
				// Create tile with pixel and position X,Y
				SwapTile(x, y);
			}
		}
	}

	void SwapTile(int x, int y)
	{
		ITile tile = myGrid.GetTile(new Pos2D(x, y));
		if (tile == null)
		{
			Debug.Log(tile);
			return;
		}
		//Debug.Log(tile.MySprite.name);
		for (int i = 0; i < BiomEnv.TypeOfTile.Count; ++i)
		{
			TileBioms tileBiom = BiomEnv.TypeOfTile[i];
			//Debug.Log(tileBiom.TypeTile);
			foreach(Biom b in tileBiom.Bioms)
			{
				if (b.TypeBiom == BiomSelected)
				{
					//Debug.Log(b.TypeBiom);
					foreach(BiomSprite bs in b.Sprites)
					{
						if (bs != null && bs.Tile.name == tile.MySprite.name)
						{
							//Debug.Log(bs.Tile.name);
							tile.MySprite = bs.Tile;
						}
					}
				}
			}				
		}
	}

	public void Init()
	{
		// TODO : random biom & map
		idTypeBiom = RandomManager.Range(0, BiomEnv.TypeOfBiom.Count - 2);
		BiomSelected = BiomEnv.TypeOfBiom[idTypeBiom];

		HeightMap = ListMaps[RandomManager.Range(0, ListMaps.Length - 2)];

		Init(BiomSelected);
	}

	public void Init(string biom)
	{
		// Pixel => Tile
		m_countTileX = HeightMap.width;
		m_countTileY = HeightMap.height;
		
		m_trans = transform;
		
		// create all Parents
		Parents = new Transform[BiomEnv.TypeOfTile.Count];
		counter = new int[BiomEnv.TypeOfTile.Count];
		
		for (int i = 0; i < Parents.Length; ++i)
		{
			GameObject obj = new GameObject();
			obj.name = BiomEnv.TypeOfTile[i].TypeTile;
			obj.transform.parent = m_trans;
			Parents[i] = obj.transform;
		}
		
		// GRID
		myGrid = new Grid(m_countTileX, m_countTileY, SizeTileX, SizeTileY);
		BiomSelected = biom;
	}

	public void BuildMap()
	{
		// getPixel on texture
		Color[] colors = HeightMap.GetPixels();
		Color pixel;

		int index = 0;
		for (int y= 0; y < m_countTileY; ++y) // WIDTH
		{
			for (int x = 0; x < m_countTileX; ++x) // HEIGHT
			{
				index = x + (m_countTileX * y);
				//TODO : check color of pixel and create the sprite
				pixel = colors[index];
				// Create tile with pixel and position X,Y
				CreateTile(pixel, x, y);
			}
		}

		// Rename Parents
		for (int i = 0; i < Parents.Length; ++i)
		{
			Parents[i].name += "_" + counter[i];
		}

		// Center
		//m_trans.position = new Vector3( -m_countTileX * SizeTileX / 2 , -m_countTileY * SizeTileY / 2, 0);
	}

	public void ClearMap()
	{
		foreach(Transform trans in Parents)
			DestroyImmediate(trans.gameObject);
		m_trans.position = Vector3.zero;
	}

	private void CreateTile(Color pixel, int x, int y)
	{
//		for (int i = 0; i < ListOfTile.Count; ++i)
//		{
//			Test pair = ListOfTile[i];
//			if (pair.color == pixel)
//			{
//				ITile tilePrefab = pair.tile;
//				Pos2D pos = new Pos2D(x, y);
//
//				ITile tile = InstantiateOnGrid(pos, tilePrefab, Parents[i], tilePrefab.name + counter[i]);
//				++counter[i];
//
//				myGrid.AddLayer(tile, pos);
//			}
//		}
		for (int i = 0; i < BiomEnv.TypeOfTile.Count; ++i)
		{
			TileBioms tileBiom = BiomEnv.TypeOfTile[i];
			if (tileBiom.PixelColor == pixel)
			{
				Pos2D pos = new Pos2D(x, y);
				
				foreach(Biom b in tileBiom.Bioms)
				{
					if (b.TypeBiom == BiomSelected)
					{
						int rand = RandomManager.Range(0, 100);
						foreach(BiomSprite bs in b.Sprites)
						{
							if (bs.Pourcent >= rand)
							{
								ITile tile = InstantiateBiom(pos, bs, Parents[i], tileBiom.TypeTile + counter[i]);
								++counter[i];
								
								myGrid.AddLayer(tile, pos);
								break;
							}
						}
					}
				}

			}
		}
	}

	public ITile InstantiateBiom(Pos2D pos, BiomSprite biom, Transform parent, string name)
	{
		GameObject go = new GameObject();
		SpriteRenderer render = go.AddComponent<SpriteRenderer>();
		render.sprite = biom.Tile;
		render.sortingOrder = biom.Zorder;
		if (biom.Special != null)
			render.material = biom.Special;

		ITile t = go.AddComponent<ITile>();
		t.Walkable = biom.Walkable;

		t.PositionGrid = pos;
		go.transform.parent = parent;
		go.name = name;
		return t;
	}

	public ITile InstantiateOnGrid(Pos2D pos, ITile tile, Transform parent, string name)
	{
		ITile go = Instantiate(tile) as ITile;
		//go.Position = myGrid.GridToWorld(pos);
		go.PositionGrid = pos;
		go.transform.parent = parent;
		go.name = name;
		return go;
	}

	public IPlayer InstantiatePlayerOnGrid(IPlayer player, ITile tile, Transform parent)
	{
		IPlayer go = Instantiate(player) as IPlayer;
		go.SetSpawn(tile);
		go.transform.parent = parent;
		return go;
	}

	public ITile InstantiateRandomOnGrid(Pos2D rand, ITile tile, Transform parent)
	{
		Pos2D realpos = new Pos2D();
		realpos.X = (RandomManager.Range(rand.X, rand.Y) * myGrid.width) / 100;
		realpos.Y = RandomManager.Range(0, myGrid.height);
		ITile randomTile = myGrid.GetTile(realpos);
		bool good = randomTile != null && randomTile.Walkable;
		int count = 0;

		while (!good)
		{
			if (count > 100)
				break;
			realpos.X = (RandomManager.Range(rand.X, rand.Y) * myGrid.width) / 100;
			realpos.Y = RandomManager.Range(0, myGrid.height);
			randomTile = myGrid.GetTile(realpos);
			good = randomTile != null && randomTile.Walkable;
			++count;
		}

		ITile go = Instantiate(tile) as ITile;
		go.PositionGrid = realpos;
		go.transform.parent = parent;
		return go;
	}
}
