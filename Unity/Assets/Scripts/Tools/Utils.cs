﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Utils 
{
	public static GameObject InstantiateChild(GameObject prefab, Transform parent)
	{
		GameObject obj = GameObject.Instantiate(prefab) as GameObject;
		obj.transform.parent = parent;
		obj.transform.localPosition = prefab.transform.localPosition;
		return obj;
	}
}
