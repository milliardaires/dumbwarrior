﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Environnement : ScriptableObject
{
	private const string DEFAULT = "Default";

	public List<string> TypeOfBiom = new List<string>();
	public List<TileBioms> TypeOfTile = new List<TileBioms>();
	
	public Material MatTile;

	public void RemoveBiom(string nameBiom)
	{
		if (nameBiom == DEFAULT)
			return;

		foreach(TileBioms type in TypeOfTile)
		{
			Biom biomToDelete = null;
			foreach(Biom biom in type.Bioms)
			{
				if (biom.TypeBiom == nameBiom)
				{
					biomToDelete = biom;
					break;
				}
			}

			if (biomToDelete != null)
				type.Bioms.Remove(biomToDelete);
		}
		TypeOfBiom.Remove(nameBiom);
	}

	public void CreateAuto(string nameBiom, string nameAtlas)
	{
		// get all sprites
		Sprite[] sprites = Resources.LoadAll<Sprite>(nameAtlas);
		Dictionary<string, Sprite> dicoSprite = new Dictionary<string, Sprite>();

		// fill the dico
		foreach(Sprite s in sprites)
		{
			if (!dicoSprite.ContainsKey(s.name))
				dicoSprite.Add(s.name, s);
		}

		TypeOfBiom.Add(nameBiom);

		// check default type of tile
		foreach(TileBioms type in TypeOfTile)
		{
			Biom _default = null; 
			foreach(Biom biom in type.Bioms)
			{
				if (biom.TypeBiom == DEFAULT)
				{
					_default = biom;
					break;
				}
			}
			if (_default != null)
			{
				Biom newBiom = new Biom(nameBiom);
				type.Bioms.Add(newBiom);
				foreach(BiomSprite sprite in _default.Sprites)
				{
					Sprite realSprite = dicoSprite[sprite.Tile.name];
					BiomSprite newSprite = new BiomSprite(realSprite);
					newSprite.Pourcent = sprite.Pourcent;
					newSprite.Walkable = sprite.Walkable;
					newSprite.Zorder = sprite.Zorder;
					newSprite.Special = sprite.Special;
					newBiom.Sprites.Add(newSprite);
				}
			}
		}
	}
}