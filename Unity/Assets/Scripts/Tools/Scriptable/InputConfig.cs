﻿using UnityEngine;
using System.Collections;

public class InputConfig : ScriptableObject
{
	public PlayerInput[] Inputs = new PlayerInput[4];
}
