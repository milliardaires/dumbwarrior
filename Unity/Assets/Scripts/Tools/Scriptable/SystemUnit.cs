﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SystemUnit : ScriptableObject
{
	public int LevelMax = 3;
	public List<Unit> Units = new List<Unit>();

	public Material MatItem;

	public void FillDico()
	{
//		m_dico.Clear();
//		foreach(Unit unit in Units)
//		{
//			m_dico.Add(unit.Name, unit.MySkills);
//		}
	}

	public int GetStat(string nameUnit, int level, DataBase.TypeStats stat)
	{
		foreach(Unit unit in Units)
		{
			if (unit.Name.Equals(nameUnit))
			//if (m_dico.ContainsKey(nameUnit))
			{

				foreach(Stats s in unit.stats)
				//foreach(Skill s in m_dico[nameUnit])
				{
					if (s.type == stat)
					{
						return s.value[level];
					}
				}
			}
		}
		return 0;
	}

	public Unit GetUnit(string nameUnit)
	{
		foreach(Unit unit in Units)
		{
			if (unit.Name.Equals(nameUnit))
			{
				return unit;
			}
		}
		return null;
	}
}
