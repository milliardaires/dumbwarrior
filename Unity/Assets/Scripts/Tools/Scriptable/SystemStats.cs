﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SystemStats : ScriptableObject
{
 	public int LevelMax = 20;
 	public List<PlayerStat> Stats = new List<PlayerStat>();

 	public void Refresh()
 	{
 		foreach(PlayerStat s in Stats)
 			s.Refresh(LevelMax);
 	}

	public int GetStatValue(int level, DataBase.TypeStats stat)
	{		
		foreach(PlayerStat s in Stats)
		{
			if (s.stat.type == stat)
			{
				return s.stat.value[level];
			}
		}
		return 0;
	}

	public PlayerStat GetStatInfo(DataBase.TypeStats stat)
	{		
		foreach(PlayerStat s in Stats)
		{
			if (s.stat.type == stat)
			{
				return s;
			}
		}
		return null;
	}
}
