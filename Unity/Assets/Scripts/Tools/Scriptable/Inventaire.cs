﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventaire : ScriptableObject
{
	public List<Item> Items = new List<Item>();
	private List<string> m_sprites = new List<string>();

	// coffres
	//private List<Item> m_containers = new List<Item>();

	public Material MatItem;
	public string Atlas;

	public void CreateAuto(string nameAtlas)
	{
		// get all sprites
		Sprite[] sprites = Resources.LoadAll<Sprite>(nameAtlas);
		//Dictionary<string, Sprite> dicoSprite = new Dictionary<string, Sprite>();
		
		// fill the dico
		foreach(Sprite s in sprites)
		{
			if (!m_sprites.Contains(s.name))
			{
				Items.Add(new Item(s));
				m_sprites.Add(s.name);
			}
		}
	}

	public void FillContainers()
	{
//		m_containers.Clear();
//		foreach(Item item in Items)
//			if (item.Container)
//				m_containers.Add(item);
	}

//	public Item GetContainer()
//	{
//
//		//return m_containers[RandomManager.Range(0, m_containers.Count - 1)];
//	}

	public void ResetList()
	{
		Items.Clear();
		m_sprites.Clear();
	}
}
