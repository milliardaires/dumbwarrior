﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Pos2D 
{
	public int X;
	public int Y;

	public Pos2D()
	{
		X = 0;
		Y = 0;
	}

	public Pos2D(int x, int y)
	{
		X = x;
		Y = y;
	}

	public Pos2D GetDecalPos(int x, int y)
	{
		Pos2D newPos = new Pos2D();
		newPos.X = X + x;
		newPos.Y = Y + y;
		return newPos;
	}

	public int Distance(Pos2D pos)
	{
		return Mathf.Abs(X - pos.X) + Mathf.Abs(Y- pos.Y);
	}

	public Pos2D Distance2D(Pos2D pos)
	{
		return new Pos2D(X - pos.X, Y- pos.Y);
	}
}

public class Layer
{
	public List<ITile> tiles; 

	public Layer(ITile tile)
	{
		tiles = new List<ITile>();
		tiles.Add(tile);
	}
}

public class Grid
{
	public int width;
	public int height;

	public int sizeX;
	public int sizeY;

	public Layer[] matrix;

	public Grid(int w, int h, int sx, int sy)
	{
		width = w;
		height = h;
		sizeX = sx;
		sizeY = sy;

		matrix = new Layer[w * h];
	}

	public Layer GetLayer(Pos2D pos)
	{
		if (pos.X < 0 || pos.Y < 0)
			return null;
		if (pos.X >= width || pos.Y >= height)
			return null;

		return matrix[pos.X + (width * pos.Y)];
	}

	void CreateLayer(ITile tile, Pos2D pos)
	{
		if (pos.X < 0 || pos.Y < 0)
			return;
		if (pos.X > width || pos.Y > height)
			return;
		
		matrix[pos.X + (width * pos.Y)] = new Layer(tile);
	}

	public ITile GetTile(Pos2D pos)
	{
		Layer layer = GetLayer(pos);
		if (layer != null)
		{
			return layer.tiles[0];
		}
		return null;
	}

	public void AddLayer(ITile tile, Pos2D pos)
	{
		Layer layer = GetLayer(pos);
		if (layer == null)
		{
			CreateLayer(tile, pos);
		}
		else
			layer.tiles.Add(tile);
	}

	public Vector3 GridToWorld(Pos2D pos)
	{
		Vector3 v = Vector3.zero;
		v.x = pos.X * sizeX;
		v.y = pos.Y * sizeY;
		return v;
	}

	public Pos2D WorldToGrid(Vector3 pos)
	{
		return null;
	}
}
