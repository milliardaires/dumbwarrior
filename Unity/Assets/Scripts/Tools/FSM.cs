﻿using System.Collections;

public class State
{
	public int ID;

	public delegate void FState();

	public FState Enter;
	public FState Logic;
	public FState Movement;
	public FState Exit;

	public State(int id, FState enterDelegate, FState logicDelegate, FState movementDelegate, FState exitDelegate)
	{
		ID = id;
		Enter = enterDelegate;
		Logic = logicDelegate;
		Movement = movementDelegate;
		Exit = exitDelegate;
	}
}

public class FSM
{
	private State m_currentState;
	private State m_lastState;
	private bool m_isChanging = false;

	public FSM()
	{
	}

	public FSM(State initState)
	{
		SetState(initState);
	}

	public void UpdateLogic()
	{
		if (m_currentState.Logic != null)
			m_currentState.Logic();
	}

	public void UpdateMovement()
	{
		if (m_currentState.Movement != null && !m_isChanging)
			m_currentState.Movement();
		m_isChanging = false;
	}

	public void ReturnState()
	{
		ChangeState(m_lastState);
	}

	public void ChangeState(State nextState)
	{
		m_currentState.Exit();
		m_lastState = m_currentState;

		SetState(nextState);
	}

//	public void ChangeStateAndUpdateLogic(State nextState)
//	{
//		m_currentState.Exit();
//		m_lastState = m_currentState;
//		
//		SetState(nextState);
//
//		UpdateLogic();
//	}

	public void SetState(State state)
	{
		m_isChanging = true;
		m_currentState = state;
		m_currentState.Enter();
	}

	public State GetState()
	{
		return m_currentState;
	}

	public State GetLastState()
	{
		return m_lastState;
	}

}
