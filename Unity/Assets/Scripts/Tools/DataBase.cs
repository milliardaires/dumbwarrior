﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataBase
{
	public enum TypeStats
	{
		DAMAGE_CAC = 0,
		DAMAGE_DIST,
		HP,
		XP,
		VISION,
		RANGE,
		DELAY,
		DIAMS,
		HEAL,
		CRIT,
		COST,
		COOLDOWN_FUFU,
		INITIATIVE,
		MAX,
	}

	public enum TypeUnits
	{
		ATTACKER = 0,
		TANK,
		RANGED,
		MAGE,
		OTHER,
		MAX,
	}

	public enum StateGame
	{
		PLAYING = 0,
		WIN,
		LOOSE,
		END,
	}

	public enum Category
	{
		COMMUN = 0,
		FER,
		OR,
		ROUGE,
		VIOLET
	}
}

[System.Serializable]
public class Stats
{
	// Type
	public DataBase.TypeStats type;
	// Value by Level
	public List<int> value = new List<int>();
	// Max value
	public int Max = 10;

	[HideInInspector]
	[System.NonSerialized]
	public bool show = false;

	#region GETTER SETTER
	//[System.NonSerialized]
	public int First
	{
		get { return value[0]; }
		set { this.value[0] = value; }
	}
	#endregion

	public Stats()
	{
		type = DataBase.TypeStats.DAMAGE_CAC;
		//value.Add(1);
	}

	public Stats(DataBase.TypeStats type)
	{
		this.type = type;
		value.Add(1);
	}

	public Stats(int levelMax)
	{
		for (int i = 0; i < levelMax; ++i)
			value.Add(i+1);
	}

	public Stats(DataBase.TypeStats type, int levelMax) : this(levelMax)
	{
		this.type = type;
	}

	public void RefreshLevel(int levelMax)
	{
		// ADD
		for (int i = value.Count; i < levelMax; ++i)
		{
			value.Add(1);
		}
		// REMOVE
		for (int i = value.Count - 1; i >= levelMax; --i)
		{
			value.RemoveAt(i);
		}
	}
}