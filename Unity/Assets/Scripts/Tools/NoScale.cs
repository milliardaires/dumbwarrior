﻿using UnityEngine;
using System.Collections;

public class NoScale : MonoBehaviour 
{
	private Transform parent;
	private Transform trans;

	// Use this for initialization
	void Start () 
	{
		trans = transform;
		parent = trans.parent;
	}
	
	// Update is called once per frame
	void Update () 
	{
		trans.localScale = parent.localScale;
	}
}
