﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerInput 
{
	public const float AXIS_TRESHOLD = 0.75f;
	public const float COOLDOWN = 0.25f;

#region Input
	[System.Serializable]
	public class ControlKey
	{
		public KeyCode Keyboard;
		public KeyCode Controller;
		public string Axis;

		private float m_timer;

		public ControlKey()
		{
			m_timer = TimeManager.CurrentTime;
		}

		public bool GetKeyUp()
		{
			return Input.GetKeyUp(Keyboard) || Input.GetKeyUp(Controller);
		}

		public bool GetKeyDown()
		{
			return Input.GetKeyDown(Keyboard) || Input.GetKeyDown(Controller);
		}

//		public float GetAxis()
//		{
//			return Input.GetAxis(Axis);
//		}

		public int GetAxis()
		{
			float time = TimeManager.CurrentTime;
			
			if (m_timer < time)
			{
				float val = Input.GetAxis(Axis);
				
				if (val > AXIS_TRESHOLD)
				{
					m_timer = time + COOLDOWN;
					return 1;
				}
				
				if (val < -AXIS_TRESHOLD)
				{
					m_timer = time + COOLDOWN;
					return -1;
				}
			}
			return 0;
		}
	}

	[System.Serializable]
	public class DirectionalInput
	{
		public ControlKey ButtonLeft;
		public ControlKey ButtonRight;
		public ControlKey ButtonUp;
		public ControlKey ButtonDown;

		public DirectionalInput()
		{}

		public bool GetLeft()
		{
			return ButtonLeft.GetKeyUp() || ButtonLeft.GetAxis() == -1;
		}

		public bool GetRight()
		{
			return ButtonRight.GetKeyUp() || ButtonRight.GetAxis() == 1;
		}

		public bool GetUp()
		{
			return ButtonUp.GetKeyUp() || ButtonUp.GetAxis() == 1;
		}

		public bool GetDown()
		{
			return ButtonDown.GetKeyUp() || ButtonDown.GetAxis() == -1;
		}
	}
#endregion
	public ControlKey ButtonA;
	public ControlKey ButtonB;
	public ControlKey ButtonStart;
	public DirectionalInput Arrows;

	public PlayerInput()
	{}

#region Press
	public bool PressA()
	{
		return ButtonA.GetKeyUp();
	}

	public bool PressB()
	{
		return ButtonB.GetKeyUp();
	}

	public bool PressStart()
	{
		return ButtonStart.GetKeyUp();
	}

	public bool PressLeft()
	{
		return Arrows.GetLeft();
	}

	public bool PressRight()
	{
		return Arrows.GetRight();
	}

	public bool PressUp()
	{
		return Arrows.GetUp();
	}

	public bool PressDown()
	{
		return Arrows.GetDown();
	}
#endregion

}
