﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Unit
{
	public string Name;
	public Sprite SpriteUI;
	public int Price;
	public DataBase.TypeUnits typeUnit;

	// LIST STATS
	public List<Stats> stats = new List<Stats>();

	[HideInInspector]
	[System.NonSerialized]
	public bool show = false;

	public bool Available = false;

	public Unit()
	{
		Name = "Farmer";
		typeUnit = DataBase.TypeUnits.OTHER;	
	}

	public Unit(string name)
	{
		Name = name;
		typeUnit = DataBase.TypeUnits.ATTACKER;	
	}

	public Unit(string name, int levelMax)
	{
		Name = name;
		stats.Add(new Stats(DataBase.TypeStats.COST, levelMax));
		stats.Add(new Stats(DataBase.TypeStats.VISION, levelMax));
		stats.Add(new Stats(DataBase.TypeStats.HP, levelMax));
	}

	public int GetStatValue(DataBase.TypeStats stat)
	{		
		return GetStatValueLevel(stat, 0);
	}

	public int GetStatValueLevel(DataBase.TypeStats stat, int level)
	{		
		foreach(Stats s in stats)
			if (s.type == stat)
				return s.value[level];
		return 0;
	}

	public Unit Clone()
	{
		return this.MemberwiseClone() as Unit;
	}
}