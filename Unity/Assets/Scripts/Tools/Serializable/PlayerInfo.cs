﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerInfo 
{
	public int Id = 0;
	public int Gold = 0;
	public int Xp = 0;
	public int Stage = 0;

	// Recap End Game
	public int Kills = 0;
	public int DiamsUsed = 0;
	public int Death = 0;
	public int XpWin = 0;

	// units
	private Unit King;
	public List<Unit> Units = new List<Unit>();
	private List<Unit> CurrentUnits = new List<Unit>();
	public int CountUnit
	{
		get { return CurrentUnits.Count; }
	}

	// items
	public List<Item> Items = new List<Item>();
	public List<Item> CurrentItems = new List<Item>();

	// stats
	public List<PlayerStat> Stats = new List<PlayerStat>();

	public bool Win = false;
	public bool Playing = false;
	public bool Selected = false;

	#region Constructor
	public PlayerInfo()
	{
	}

	public PlayerInfo(int id)
	{
		Id = id;
	}

	public PlayerInfo(int id, List<Unit> units, List<PlayerStat> stats) : this (id)
	{
		Units = units;
		Stats = stats;
	}
	#endregion

	public void Active(int stage)
	{
		Playing = true;
		Stage = stage;
	}

	public void Reset()
	{
		Playing = false;
		Win = false;

		//CurrentUnits.Clear();
		//CurrentItems.Clear();

		Kills = 0;
		Death = 0;
		DiamsUsed = 0;
		XpWin = 0;
	}

	public void EndGame(bool win, int kill, int death, int diam, int xp)
	{
		Win = win;
		Kills = kill;
		Death = death;
		DiamsUsed = diam;
		XpWin = xp;
	}

	#region Units
	public bool AddCurrentUnit(Unit unit)
	{
		if (CurrentUnits.Contains(unit))
		    return false;

		CurrentUnits.Add(unit);
		return true;
	}

	public Unit GetCurrentUnit(int i)
	{
		if (i >= CountUnit)
			return null;
		return CurrentUnits[i];
	}

	public Unit GetKing()
	{
		return King;
	}

	public void RemoveCurrentUnit(Unit unit)
	{
		CurrentUnits.Remove(unit);
	}

	public bool BuyUnit(Unit unit)
	{
		if (Gold >= unit.Price)
		{
			Gold -= unit.Price;
			unit.Available = true;
			Debug.LogWarning("to do buy unit ? " + unit.Name + " price " + unit.Price + " gold");
			return true;
		}
		return false;
	}

	public void AddUnit(Unit unit)
	{
		if (unit.Name != "King")
		{
			// TODO : decomment to have all players in game
			if (unit.Name == "Farmer")
				AddCurrentUnit(unit);
			else
				Units.Add(unit);
		}
		else
			King = unit;
	}
	#endregion

	#region Items
	public void AddCurrentItem(Item item)
	{
		CurrentItems.Add(item);
	}
	public Item GetCurrentItem(int i)
	{
		return CurrentItems[i];
	}
	public void RemoveCurrentItem(Item item)
	{
		CurrentItems.Remove(item);
	}

	public void AddNewItem(Item item)
	{
		Items.Add(item);
	}

	public void OpenContainer(Item container, Item newItem)
	{
		// replace this
		int index = Items.LastIndexOf(container);
		Items[index] = newItem;
	}

	public void SellItem(Item item)
	{
		Debug.LogWarning("to do sell item " + item.Price);
		Gold += item.Price;
		Items.Remove(item);
	}
	#endregion

	#region Stats
	public PlayerStat GetStatInfo(DataBase.TypeStats stat)
	{		
		foreach(PlayerStat s in Stats)
		{
			if (s.stat.type == stat)
			{
				return s;
			}
		}
		return null;
	}

	public bool BuyLevelUp(PlayerStat stat)
	{
		if (Gold >= stat.Price)
		{
			stat.LevelUp();
			Debug.LogWarning("level up " + stat.Level);
			Gold -= stat.Price;
			return true;
		}
		return false;
	}
	#endregion
}
