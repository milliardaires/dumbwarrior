﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TileBioms
{
	public string TypeTile;
	public List<Biom> Bioms;
	public Color PixelColor;
	// GUI
	public bool show;

	public TileBioms(string n)
	{
		TypeTile = n;
		Bioms = new List<Biom>();
		PixelColor = Color.white;
	}
}

[System.Serializable]
public class Biom
{
	public string TypeBiom;
	public List<BiomSprite> Sprites;

	// GUI
	public bool show;

	public Biom(string n)
	{
		TypeBiom = n;
		Sprites = new List<BiomSprite>();
	}
}

[System.Serializable]
public class BiomSprite
{
	public Sprite Tile;
	public int Pourcent;
	public bool Walkable = false;
	public int Zorder;
	public Material Special;

	public BiomSprite(Sprite t)
	{
		Tile = t;
		Pourcent = 100;
		Zorder = 0;
	}
}
