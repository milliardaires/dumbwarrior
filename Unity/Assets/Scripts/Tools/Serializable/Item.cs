﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Item
{
	public Sprite Icon;
	public int Price;
	public string Name;
	public string Description = "xxxxx";

	public DataBase.Category category = DataBase.Category.COMMUN;

	//Coffre
	public bool Container = false;
	public int Rate = 1;

	// LIST STATS
	public List<Stats> stats = new List<Stats>();

	[HideInInspector]
	[System.NonSerialized]
	public bool show = false;

	public Item()
	{
		Name = "item";
		for(int i = 0; i < 3; ++i)
			stats.Add(new Stats(DataBase.TypeStats.DAMAGE_CAC));
	}

	public Item(Sprite icon)
	{
		Icon = icon;
		for(int i = 0; i < 3; ++i)
			stats.Add(new Stats(DataBase.TypeStats.DAMAGE_CAC));
	}

	public int GetStatValue(DataBase.TypeStats stat)
	{		
		foreach(Stats s in stats)
			if (s.type == stat)
				return s.First;
		return 0;
	}
}
