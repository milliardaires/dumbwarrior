﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerStat
{
	private int m_level = 0;
	public int Level
	{
		get { return m_level;}
	}

	// STATS
	public int Price;
	public Stats stat;
	public string Description = "xxxx";

	[HideInInspector]
	public bool show = false;

	public PlayerStat()
	{
		stat = new Stats();
	}

	public PlayerStat(int levelMax)
	{
		stat = new Stats(levelMax);
	}

	public int GetStatValue()
	{
		return stat.value[m_level];
	}

	public int GetStatNextValue()
	{
		return stat.value[m_level < stat.Max-1 ? m_level+1 : m_level];
	}

	public void LevelUp()
	{
		if (m_level < stat.Max -1)
			++m_level;
	}

	public void Refresh(int levelMax)
	{
		stat.RefreshLevel(levelMax);
	}

	public PlayerStat Clone()
	{
		return this.MemberwiseClone() as PlayerStat;
	}
}
