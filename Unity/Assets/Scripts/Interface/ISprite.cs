﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class ISprite : MonoBehaviour 
{
	// Position in the world
	//private Vector3 position;
	public Vector3 Position
	{
		get 
		{
			if (_transform == null)
			{
				_transform = transform;
			}
			return _transform.position;
		}
		set 
		{
			if (_transform == null)
			{
				_transform = transform;
			}
			_transform.position = value;
		}
	}
	private Transform _transform;
	public Vector3 Scale
	{
		get 
		{
			if (_transform == null)
			{
				_transform = transform;
			}
			return _transform.localScale;
		}
		set 
		{
			if (_transform == null)
			{
				_transform = transform;
			}
			_transform.localScale = value;
		}
	}

	protected SpriteRenderer m_spriteRenderer;
	public SpriteRenderer SpriteRender
	{
		get 
		{
			if (m_spriteRenderer == null)
			{
				m_spriteRenderer = GetComponent<SpriteRenderer>();
			}
			return m_spriteRenderer;
		}
	}


	public Sprite MySprite
	{
		get 
		{
			return SpriteRender.sprite;
		}
		set 
		{
			SpriteRender.sprite = value;
		}
	}
	// Size Block
	public int SizeX;
	public int SixeY;
	// Z Order
	public int Depth;

}
