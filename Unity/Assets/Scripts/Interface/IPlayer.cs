﻿using UnityEngine;
using System.Collections;
using System;

public class IPlayer : IMovable , ITicked
{
	protected const char DELIMIT = '_';

	#region Stats
	public int MaxLevel = 3;
	protected int m_currentLevel = 1;

	// % chance de voir un ennemie
	public int Vision = 5;
	// % chance de critique : fait reculer d'une case l'ennemie
	public int Critique = 1;
	public int Range = 1;
	public int HP = 1;
	public bool FullLife
	{
		get { return HP == m_currentHP;}
	}
	public int Damage = 1;
	protected int m_currentHP;

	protected int m_initiative = 100;
	public int Initiative
	{
		get { return m_initiative;}
	}

	#endregion

	#region UI
	public Transform LifeBar;
	public Transform LifeZone;

	#endregion

	#region FX
	public GameObject FX_Hit;
	#endregion

	#region Tracking
	protected IPlayer m_targetEnemy;
	protected bool m_attackOnTick = false;
	public bool TickAttack
	{
		get { return m_attackOnTick; }
	}
	#endregion

	public bool InFight = false;
	public bool IsDead = false;
	public bool IsVisible = true;
	protected bool TakeDamage = false;

	#region Diamon
	public event Action OnCatchDiamon;
	protected PoolDiamon m_poolDiamon;
	protected void AddDiamon()
	{
		if (OnCatchDiamon != null)
			OnCatchDiamon();
	}
	public ITile Treasure;
	#endregion

	protected PlayersManager m_playerMgr;

	#region Functions
	public virtual void Init(PlayersManager player, Unit unit, int[] bonus, int levelUnit)
	{
		m_playerMgr = player;
		m_poolDiamon = m_playerMgr.Pooler;
		RightSide = m_playerMgr.RightSide;

		SpriteRender.material = m_playerMgr.PlayerColor;
		LifeBar.renderer.material.color = m_playerMgr.PlayerColor.GetColor("_MaskColor");

		HP = unit.GetStatValueLevel(DataBase.TypeStats.HP, levelUnit) + bonus[(int)DataBase.TypeStats.HP];
		Vision = unit.GetStatValueLevel(DataBase.TypeStats.VISION, levelUnit) + bonus[(int)DataBase.TypeStats.VISION];
		Range = unit.GetStatValueLevel(DataBase.TypeStats.RANGE, levelUnit) + bonus[(int)DataBase.TypeStats.RANGE];
		if (Range <= 1)
			Damage = unit.GetStatValueLevel(DataBase.TypeStats.DAMAGE_CAC, levelUnit) + bonus[(int)DataBase.TypeStats.DAMAGE_CAC];
		else
			Damage = unit.GetStatValueLevel(DataBase.TypeStats.DAMAGE_DIST, levelUnit) + bonus[(int)DataBase.TypeStats.DAMAGE_DIST];
		Critique = unit.GetStatValueLevel(DataBase.TypeStats.CRIT, levelUnit) + bonus[(int)DataBase.TypeStats.CRIT];
		//Heal = unit.GetStatValue(DataBase.TypeStats.HEAL) + bonus[(int)DataBase.TypeStats.HEAL];

		m_currentLevel = levelUnit + 1;
		m_currentHP = HP;

		InitAnimation();

		InitFSM();
	}

	public virtual void InitFSM()
	{
		Debug.Log("fsm");
	}

	public virtual void InitAnimation()
	{
		Debug.Log("anim");
	}

	public virtual void OnLogicTick()
	{
	}
	public virtual void OnMovementTick()
	{
	}

	public virtual void Subscribe()
	{
		GameTick.Instance.UpdateLogic += OnLogicTick;
		GameTick.Instance.UpdateMovement += OnMovementTick;
	}

	public virtual void UnSubscribe()
	{
		if (GameTick.Instance != null)
		{
			GameTick.Instance.UpdateLogic -= OnLogicTick;
			GameTick.Instance.UpdateMovement -= OnMovementTick;
		}
	}

	virtual public void ReceiveHeal(int heal)
	{
		m_currentHP = Mathf.Clamp(m_currentHP + heal, 0, HP);

		Vector3 scaleLife = LifeBar.localScale;
		scaleLife.x = Mathf.Clamp01( (float)m_currentHP / (float)HP);
		LifeBar.localScale = scaleLife;

		SpriteRender.color = Color.green;
		Invoke("ResetColor", 0.1f);
	}

	virtual public bool ReceiveDamage(int damage)
	{
		//IsAttack();
		m_currentHP -= damage;
		TakeDamage = true;
		//Debug.Log(gameObject.name + " hp " + m_currentHP);
		Vector3 scaleLife = LifeBar.localScale;
		scaleLife.x = Mathf.Clamp01( (float)m_currentHP / (float)HP);
		LifeBar.localScale = scaleLife;

		if (m_currentHP <= 0)
		{
			GoDie();
			//LifeZone.localScale = scaleLife;
			Destroy(LifeZone.gameObject);
		}

		if (TakeDamage) 
		{			
			SpriteRender.color = Color.red;
			Invoke("ResetColor", 0.1f);
			GameObject obj = Utils.InstantiateChild(FX_Hit, transform);
			Destroy(obj, 3f);	
		}

		return IsDead;
	}

	void ResetColor()
	{
		SpriteRender.color = Color.white;
	}

	virtual public void GoDie()
	{
		m_playerMgr.UnitDie(this);
		IsDead = true;
	}

	virtual public void IsAttackBy(IPlayer player)
	{
	}

//	virtual public void LevelUp()
//	{
//		++m_currentLevel;
//	}

	protected bool HasHole(ITile tile)
	{
		ITile t;
		for( int i = 0; i < MaxDirection; i++)
		{
			t = tile.GetNeighbours(i);
			if( t != null)
			{
				if (t.CanGo && t != Tile)
					return false;
			}
		}
		return true;
	}

	protected bool ComputeRange(ITile target)
	{
		int distance = Tile.PositionGrid.Distance(target.PositionGrid);

		// TODO : distance = 1 ( contact )
		if (distance <= Range)
			return true;
		else
			return false;
	}

	protected void ReplaceSprite(ITile target)
	{
		Pos2D dist = Tile.PositionGrid.Distance2D(target.PositionGrid);
		if (dist.X > 0)
		{
			Scale = new Vector3(-1, 1, 1);
		}
		else if (dist.X < 0)
		{
			Scale = Vector3.one;
		}
	}

	#endregion
}
