﻿using UnityEngine;
using System.Collections;

public class ITile : ISprite 
{
	public Pos2D m_pos2D;
	public Pos2D PositionGrid
	{
		get { return m_pos2D; }
		set 
		{
			m_pos2D = value;
			Position = TileMapper.Instance.GRID.GridToWorld(m_pos2D);
		}
	}

	// Walkable or not
	public bool Walkable;

	private bool m_taken = false;
	public bool Taken
	{
		get { return m_taken; }
		set { m_taken = value; }
	}

	public bool CanGo
	{
		get { return Walkable && !m_taken; }
	}

	private Pos2D[] direction = {new Pos2D(0, 1), new Pos2D(1, 0), new Pos2D(0, -1), new Pos2D(-1, 0)};
	
	virtual public ITile GetUp() 
	{
		return GetNeighbours(0);
	}
	virtual public ITile GetRight() 
	{
		return GetNeighbours(1);
	}
	virtual public ITile GetDown() 
	{
		return GetNeighbours(2);
	}
	virtual public ITile GetLeft() 
	{
		return GetNeighbours(3);
	}

	public ITile GetNeighbours(int i)
	{
		return TileMapper.Instance.GRID.GetTile(PositionGrid.GetDecalPos(direction[i].X, direction[i].Y));
	}
}