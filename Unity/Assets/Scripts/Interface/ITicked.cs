﻿// Updated by tick
public interface ITicked 
{
	void Subscribe();
	void OnLogicTick();
	void OnMovementTick();
	void UnSubscribe();
}
