﻿using UnityEngine;
using System.Collections;

public class IMovable : ISprite 
{
	// Tile
	public ITile Tile;
	private ITile m_lastTile;
	public ITile LastTile
	{
		get { return m_lastTile;}
		set { m_lastTile = value;}
	}

	public enum Direction
	{
	 	UP = 0,
	 	RIGHT = 1,
	 	DOWN = 2,
		LEFT = 3,
		MAX = 4
	}
	public int MaxDirection
	{
		get { return (int) Direction.MAX; }
	}

	public bool RightSide = false;
	//public Vector2 RangeNormalMovement;
	public Vector2 RangeQuickMovement;
	public Vector2 RangeSlowMovement;

	protected float ProbaLeft;
	protected float ProbaRight;
	protected float ProbaDown;
	protected float ProbaUp;
	protected const float PROBA_INIT = 0.25f;
	protected const float PROBA_LAST_TILE = 0.1f;

	public AnimationCurve Curve;
	protected const float HEIGHT_JUMP = 5f;

	#region Functions
	public void SetSpawn(ITile tile)
	{
		Tile = tile;
		Position = Tile.Position;
	}

	public void SetTile(ITile tile)
	{
		MoveTo(tile);
	}

	virtual public void GoLeft() 
	{
		MoveTo(Tile.GetLeft());
		Scale = new Vector3(-1, 1, 1);
	}
	virtual public void GoRight() 
	{
		MoveTo(Tile.GetRight());
		Scale = Vector3.one;
	}
	virtual public void GoUp() 
	{
		MoveTo(Tile.GetUp());
	}
	virtual public void GoDown() 
	{
		MoveTo(Tile.GetDown());
	}

	virtual public void GoNeighbour(int i) 
	{
		MoveTo(Tile.GetNeighbours(i));
		if (i == 3)
		{
			Scale = new Vector3(-1, 1, 1);
		}
		else if (i == 1)
		{
			Scale = Vector3.one;
		}
	}

	void MoveTo(ITile tile)
	{
		if (tile != null)
		{
			if (tile.CanGo)
			{
				if (Tile != null)
				{
					m_lastTile = Tile;
					m_lastTile.Taken = false;
				}
				Tile = tile;
				Tile.Taken = true;
				//Position = Tile.Position;
				StartCoroutine(MovingTo(Tile.Position));
			}
			else
				Debug.Log("no walkable");
		}
		else
			Debug.Log("tile null");

	}

	IEnumerator MovingTo(Vector3 posFinal)
	{
		bool movVertical = false;
		if ( Mathf.Abs((posFinal - Position).y) > 1)
		{
			movVertical = true;
		}

		Vector3 from = Position;
		float x = from.x;
		float y = from.y;

		float t = 0f;
		while (t < 1f)
		{
			t += TimeManager.DeltaTime / (GameTick.Instance.TimeByTick * GameTick.Instance.TimeByTick);

			if (movVertical)
			{
				Position = Vector3.Lerp(from, posFinal, t);
			}
			else
			{
				x = Mathf.Lerp(from.x, posFinal.x, t);
				y = from.y + (Curve.Evaluate(t) * HEIGHT_JUMP);
				Position = new Vector3(x, y, from.z);
			}

			yield return new WaitForEndOfFrame();
		}
	}

	private float GetFactorMovement(int posX)
	{
		if (RangeQuickMovement.x < posX && posX < RangeQuickMovement.y)
		{
			float x = (posX - RangeQuickMovement.x) / (RangeQuickMovement.y - RangeQuickMovement.x);// [0,1]
			return (4f * (1 - x)) + 1; // [2,1]
		}
		else if (RangeSlowMovement.x < posX && posX < RangeSlowMovement.y)
		{
			float x = (posX - RangeSlowMovement.x) / (RangeSlowMovement.y - RangeSlowMovement.x);// [0,1]
			return (1 - x); // [1,0]
		}
		else
		{
			return 1f;
		}

	}

	protected void ComputeRandomMovement()
	{
		// random [0,1]
		float rand = RandomManager.Range(0f, 1f);

		// test if there are a tile
		ITile t = Tile.GetLeft();
		float freeLeft = t != null ? (t.CanGo == false ? 0 : 1) : 0;
		t = Tile.GetRight();
		float freeRight = t != null ? (t.CanGo == false ? 0 : 1) : 0;
		t = Tile.GetUp();
		float freeUp = t != null ? (t.CanGo == false ? 0 : 1) : 0;
		t = Tile.GetDown();
		float freeDown = t != null ? (t.CanGo == false ? 0 : 1) : 0;

		// average probability
		float somme = freeLeft + freeRight + freeUp + freeDown;
		if (somme > 1)
		{
			freeLeft *=  Tile.GetLeft() != LastTile ? 1 : PROBA_LAST_TILE;
			freeRight *= Tile.GetRight() != LastTile ? 1 : PROBA_LAST_TILE;
			freeUp *= Tile.GetUp() != LastTile ? 1 : PROBA_LAST_TILE;
			freeDown *= Tile.GetDown() != LastTile ? 1 : PROBA_LAST_TILE;
		}
		somme = freeLeft + freeRight + freeUp + freeDown;
		//Debug.Log(somme + " = " + freeLeft + " + " + freeRight + " + " + freeUp + " + " + freeDown);  
		float probaInit = Mathf.Clamp01(1f / somme);

		if (RightSide)
		{
			int x = ((TileMapper.Instance.GRID.width - Tile.PositionGrid.X) * 100) / TileMapper.Instance.GRID.width; //[0,100]

			ProbaLeft = freeLeft * probaInit * GetFactorMovement(x); // TODO : pourcent of deplacement

			float newProba = Mathf.Clamp((1 - ProbaLeft) / (somme - freeLeft), 0, 1);
			ProbaRight = freeRight * newProba;
			ProbaDown = freeDown * newProba; 
			ProbaUp = freeUp * newProba;
		}
		else
		{
			// position on the grid
			int x = (Tile.PositionGrid.X * 100) / TileMapper.Instance.GRID.width; //[0,100]

			ProbaRight = freeRight * probaInit * GetFactorMovement(x);
			//Debug.Log(ProbaRight + " " + freeRight);

			float newProba = Mathf.Clamp((1 - ProbaRight) / (somme - freeRight), 0, 1);
			ProbaLeft = freeLeft * newProba;
			ProbaDown = freeDown * newProba; 
			ProbaUp = freeUp * newProba;
		}
		
		float probaH = ProbaLeft + ProbaRight;
		float probaV = probaH + ProbaDown;
		
		if (rand < ProbaLeft) // [0, left]
		{
			GoLeft();
		}
		else if (ProbaLeft < rand && rand < probaH)
		{
			GoRight();
		}
		else if (probaH < rand && rand <  probaV)
		{
			GoDown();
		}
		else if (probaV < rand) // [proba, 1]
		{
			GoUp();
		}
	}

//	void Update()
//	{
//		InputDebug();
//	}
//
//	void InputDebug()
//	{
//		if (Input.GetKeyDown("left"))
//			GoLeft();
//		if (Input.GetKeyDown("right"))
//			GoRight();
//		if (Input.GetKeyDown("down"))
//			GoDown();
//		if (Input.GetKeyDown("up"))
//			GoUp();
//	}
	#endregion
}