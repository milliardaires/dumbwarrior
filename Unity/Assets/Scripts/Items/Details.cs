﻿using UnityEngine;
using System.Collections;

public class Details : ITile 
{
	[System.Serializable]
	public class DetailsPourcent
	{
		public int Pourcent;
		public Sprite Sprite;
	}

	public DetailsPourcent[] Detail;

	// Use this for initialization
	void Start () 
	{
		int rand = RandomManager.Range(0, 100);
		foreach(DetailsPourcent detail in Detail)
		{
			if (detail.Pourcent > rand)
			{
				MySprite = detail.Sprite;
				break;
			}
		}
	}

}
