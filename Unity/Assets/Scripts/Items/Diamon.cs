﻿using UnityEngine;
using System.Collections;
using System;

public class Diamon : ITile 
{
	public event Action<Diamon> OnCatch;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	public void Catched() 
	{
		if (OnCatch != null)
			OnCatch(this);
	}
}
