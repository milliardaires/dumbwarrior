﻿using UnityEngine;
using System.Collections;

public class Spawn : ITile
{
	public int MaxSpawn;
	public bool AlreadySpawn;

	void OnEnable()
	{
		Taken = true;
		//Subscribe();
	}
//
//	void OnDisable()
//	{
//		UnSubscribe();
//	}

	public IPlayer SpawnPlayer(IPlayer player)
	{
		// TODO : spawn here
		return TileMapper.Instance.InstantiatePlayerOnGrid(player, (ITile)this, transform);
	}
}
