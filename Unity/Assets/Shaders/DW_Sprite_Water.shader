Shader "DW/Sprite_Water"
{
	Properties 
	{
_MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}
_FX("_FX", 2D) = "black" {}
_R("_R", Vector) = (0,0,0,0)
_G("_G", Vector) = (0,0,0,0)
_B("_B", Vector) = (0,0,0,0)
_R_Color("_R_Color", Color) = (1,1,1,1)
_G_Color("_G_Color", Color) = (1,1,1,1)
_B_Color("_B_Color", Color) = (1,1,1,1)

	}
	
	SubShader 
	{
		Tags
		{
"Queue"="Transparent"
"IgnoreProjector"="False"
"RenderType"="Transparent"

		}

		
Cull Back
ZWrite On
ZTest LEqual
ColorMask RGBA
Fog{
}


		CGPROGRAM
#pragma surface surf BlinnPhongEditor  alpha decal:blend vertex:vert
#pragma target 2.0


sampler2D _MainTex;
sampler2D _FX;
float4 _R;
float4 _G;
float4 _B;
float4 _R_Color;
float4 _G_Color;
float4 _B_Color;

			struct EditorSurfaceOutput {
				half3 Albedo;
				half3 Normal;
				half3 Emission;
				half3 Gloss;
				half Specular;
				half Alpha;
				half4 Custom;
			};
			
			inline half4 LightingBlinnPhongEditor_PrePass (EditorSurfaceOutput s, half4 light)
			{
half3 spec = light.a * s.Gloss;
half4 c;
c.rgb = (s.Albedo * light.rgb + light.rgb * spec);
c.a = s.Alpha;
return c;

			}

			inline half4 LightingBlinnPhongEditor (EditorSurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
			{
				half3 h = normalize (lightDir + viewDir);
				
				half diff = max (0, dot ( lightDir, s.Normal ));
				
				float nh = max (0, dot (s.Normal, h));
				float spec = pow (nh, s.Specular*128.0);
				
				half4 res;
				res.rgb = _LightColor0.rgb * diff;
				res.w = spec * Luminance (_LightColor0.rgb);
				res *= atten * 2.0;

				return LightingBlinnPhongEditor_PrePass( s, res );
			}
			
			struct Input {
				float2 uv_MainTex;
float4 screenPos;

			};

			void vert (inout appdata_full v, out Input o) {
float4 VertexOutputMaster0_0_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_1_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_2_NoInput = float4(0,0,0,0);
float4 VertexOutputMaster0_3_NoInput = float4(0,0,0,0);


			}
			

			void surf (Input IN, inout EditorSurfaceOutput o) {
				o.Normal = float3(0.0,0.0,1.0);
				o.Alpha = 1.0;
				o.Albedo = 0.0;
				o.Emission = 0.0;
				o.Gloss = 0.0;
				o.Specular = 0.0;
				o.Custom = 0.0;
				
float4 Sampled2D0=tex2D(_MainTex,IN.uv_MainTex.xy);
float4 Split7=IN.screenPos;
float4 Assemble3=float4(float4( Split7.x, Split7.x, Split7.x, Split7.x).x, float4( Split7.y, Split7.y, Split7.y, Split7.y).y, float4( Split7.z, Split7.z, Split7.z, Split7.z).z, float4( Split7.w, Split7.w, Split7.w, Split7.w).w);
float4 Split1=Assemble3;
float4 Split4=_R;
float4 Multiply0=float4( Split4.x, Split4.x, Split4.x, Split4.x) * _Time;
float4 UV_Pan0=float4(float4( Split1.x, Split1.x, Split1.x, Split1.x).x + Multiply0.x,float4( Split1.x, Split1.x, Split1.x, Split1.x).y,float4( Split1.x, Split1.x, Split1.x, Split1.x).z,float4( Split1.x, Split1.x, Split1.x, Split1.x).w);
float4 Multiply10=UV_Pan0 * float4( Split4.z, Split4.z, Split4.z, Split4.z);
float4 Multiply1=float4( Split4.y, Split4.y, Split4.y, Split4.y) * _Time;
float4 UV_Pan1=float4(float4( Split1.y, Split1.y, Split1.y, Split1.y).x + Multiply1.x,float4( Split1.y, Split1.y, Split1.y, Split1.y).y,float4( Split1.y, Split1.y, Split1.y, Split1.y).z,float4( Split1.y, Split1.y, Split1.y, Split1.y).w);
float4 Multiply12=UV_Pan1 * float4( Split4.w, Split4.w, Split4.w, Split4.w);
float4 Assemble0_2_NoInput = float4(0,0,0,0);
float4 Assemble0_3_NoInput = float4(0,0,0,0);
float4 Assemble0=float4(Multiply10.x, Multiply12.y, Assemble0_2_NoInput.z, Assemble0_3_NoInput.w);
float4 Tex2D1=tex2D(_FX,Assemble0.xy);
float4 Splat6=Tex2D1.x;
float4 Splat9=_R_Color.w;
float4 Multiply6=Splat6 * Splat9;
float4 Lerp0=lerp(Sampled2D0,_R_Color,Multiply6);
float4 Split6=_G;
float4 Split5=Assemble3;
float4 Multiply9=float4( Split6.x, Split6.x, Split6.x, Split6.x) * _Time;
float4 UV_Pan4=float4(float4( Split5.x, Split5.x, Split5.x, Split5.x).x + Multiply9.x,float4( Split5.x, Split5.x, Split5.x, Split5.x).y,float4( Split5.x, Split5.x, Split5.x, Split5.x).z,float4( Split5.x, Split5.x, Split5.x, Split5.x).w);
float4 Multiply13=float4( Split6.z, Split6.z, Split6.z, Split6.z) * UV_Pan4;
float4 Multiply11=_Time * float4( Split6.y, Split6.y, Split6.y, Split6.y);
float4 UV_Pan5=float4(float4( Split5.y, Split5.y, Split5.y, Split5.y).x + Multiply11.x,float4( Split5.y, Split5.y, Split5.y, Split5.y).y,float4( Split5.y, Split5.y, Split5.y, Split5.y).z,float4( Split5.y, Split5.y, Split5.y, Split5.y).w);
float4 Multiply14=UV_Pan5 * float4( Split6.w, Split6.w, Split6.w, Split6.w);
float4 Assemble2_2_NoInput = float4(0,0,0,0);
float4 Assemble2_3_NoInput = float4(0,0,0,0);
float4 Assemble2=float4(Multiply13.x, Multiply14.y, Assemble2_2_NoInput.z, Assemble2_3_NoInput.w);
float4 Tex2D0=tex2D(_FX,Assemble2.xy);
float4 Splat7=Tex2D0.y;
float4 Splat10=_G_Color.w;
float4 Multiply7=Splat7 * Splat10;
float4 Lerp1=lerp(Lerp0,_G_Color,Multiply7);
float4 Split3=_B;
float4 Split2=Assemble3;
float4 Multiply2=float4( Split3.x, Split3.x, Split3.x, Split3.x) * _Time;
float4 UV_Pan2=float4(float4( Split2.x, Split2.x, Split2.x, Split2.x).x + Multiply2.x,float4( Split2.x, Split2.x, Split2.x, Split2.x).y,float4( Split2.x, Split2.x, Split2.x, Split2.x).z,float4( Split2.x, Split2.x, Split2.x, Split2.x).w);
float4 Multiply4=float4( Split3.z, Split3.z, Split3.z, Split3.z) * UV_Pan2;
float4 Multiply3=_Time * float4( Split3.y, Split3.y, Split3.y, Split3.y);
float4 UV_Pan3=float4(float4( Split2.y, Split2.y, Split2.y, Split2.y).x + Multiply3.x,float4( Split2.y, Split2.y, Split2.y, Split2.y).y,float4( Split2.y, Split2.y, Split2.y, Split2.y).z,float4( Split2.y, Split2.y, Split2.y, Split2.y).w);
float4 Multiply5=UV_Pan3 * float4( Split3.w, Split3.w, Split3.w, Split3.w);
float4 Assemble1_2_NoInput = float4(0,0,0,0);
float4 Assemble1_3_NoInput = float4(0,0,0,0);
float4 Assemble1=float4(Multiply4.x, Multiply5.y, Assemble1_2_NoInput.z, Assemble1_3_NoInput.w);
float4 Tex2D2=tex2D(_FX,Assemble1.xy);
float4 Splat8=Tex2D2.z;
float4 Splat11=_B_Color.w;
float4 Multiply8=Splat8 * Splat11;
float4 Lerp2=lerp(Lerp1,_B_Color,Multiply8);
float4 Master0_0_NoInput = float4(0,0,0,0);
float4 Master0_1_NoInput = float4(0,0,1,1);
float4 Master0_3_NoInput = float4(0,0,0,0);
float4 Master0_4_NoInput = float4(0,0,0,0);
float4 Master0_7_NoInput = float4(0,0,0,0);
float4 Master0_6_NoInput = float4(1,1,1,1);
o.Emission = Lerp2;
o.Alpha = Sampled2D0.aaaa;

				o.Normal = normalize(o.Normal);
			}
		ENDCG
	}
	Fallback "Diffuse"
}